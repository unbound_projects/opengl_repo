#include "TextureApp.h"
#include "../Helper.h"
#include <stb-master\stb-master\stb_image.h>
#include <assert.h>
#include <glfw3.h>
#include <glm\vec3.hpp>
#include <glm\mat4x4.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <cmath>

TextureApp::TextureApp()
{

}

TextureApp::~TextureApp()
{

}

bool TextureApp::Initialise()
{
	// creating variables to store the loaded images height, width & format
	int imageWidth = 0, imageHeight = 0, imageFormat = 0;

	/*
	******* SETTING UP THE NORMAL MAP ****************
	*/

	// load in the normal map
	unsigned char* data = stbi_load("../data/textures/rock_normal.tga", &imageWidth, &imageHeight, &imageFormat, STBI_default);

	// generate the normal map
	glGenTextures(1, &m_normalmap);

	// bind the texture to the normal map
	glBindTexture(GL_TEXTURE_2D, m_normalmap);

	// specify the data for the texture, including the format/resolution/variable type
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageWidth, imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

	// specifying the filtering so that openGL can read the image at various res
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	stbi_image_free(data);

	/*
		******* SETTING UP THE DIFFUSE MAP ****************
	*/

	// load the diffuse map
	data = stbi_load("../data/textures/rock_diffuse.tga", 
		&imageWidth, &imageHeight, &imageFormat, STBI_default);


	// generate the texture
	glGenTextures(1, &m_texture);

	// bind the texture to the correct slot, in this case 2-D
	glBindTexture(GL_TEXTURE_2D, m_texture);

	// specify the data for the texture, including the format/resolution/variable type
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageWidth, imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

	// specifying the filtering so that openGL can read the image at various res
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	stbi_image_free(data);

	/*
	******* SETTING UP THE VERTEX SHADER ****************
	*/
	const char* vertexShaderSource =
		R"(
			#version 410
			layout(location = 0) in vec4 Position;
			layout(location = 1) in vec2 textureCoord;
			layout(location = 2) in vec4 Normal;
			layout(location = 3) in vec4 Tangent;

			out vec2 vTexCoord;
			out vec3 vNormal;
			out vec3 vTangent;
			out vec3 vBiTangent;

			uniform mat4 ProjectionView;
			
			void main() 
			{
				vTexCoord = textureCoord;
				vNormal = Normal.xyz;
				vTangent = Tangent.xyz;
				vBiTangent = cross(vNormal, vTangent);
				gl_Position = ProjectionView * Position;
			};
		)";

	/*
	******* SETTING UP THE FRAGMENT SHADER ****************
	*/
	const char* fragmentShaderSource =
		R"(
			#version 410
			in vec2 vTexCoord;
			in vec3 vNormal;
			in vec3 vTangent;
			in vec3 vBiTangent;
			out vec4 FragColour;
			uniform vec3 lightDir;
			uniform sampler2D diffuse; 
			uniform sampler2D normal;
			
			void main() 
			{ 
				mat3 TBN = mat3( normalize(vTangent), normalize(vBiTangent), normalize(vNormal));
				vec3 N = texture(normal, vTexCoord).xyz * 2 - 1;
				float d = max(0, dot(normalize(TBN * N), normalize(lightDir)));
				FragColour = texture(diffuse, vTexCoord);
				FragColour.rgb = FragColour.rgb * d;
			}
		)";
	if (!m_program.Create(vertexShaderSource, fragmentShaderSource))
	{
		return false;
	}
	
	GenerateTexture(m_VBO, m_IBO, m_VAO);

	return true;
}

void TextureApp::DeInitialise()
{

}

bool TextureApp::Update(float deltaTime)
{
	return true;
}

void TextureApp::Draw(glm::mat4 projectionView)
{
	GLuint programID = m_program.GetID();

	// use our texture program
	glUseProgram(programID);

	// bind the camera & assert if the location can't be found
	int iLocation = glGetUniformLocation(programID, "ProjectionView");
	assert(iLocation != -1 && "If -1, could not find this uniform in the given shader.");
	glUniformMatrix4fv(iLocation, 1, GL_FALSE, glm::value_ptr(projectionView));

	// set texture slot
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_texture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_normalmap);
	
	// tell the shader where the diffuse map is
	iLocation = glGetUniformLocation(programID, "diffuse");
	assert(iLocation != -1 && "If -1, could not find this uniform in the given shader.");
	glUniform1i(iLocation, 0);

	// tell the shader where the normal map is
	iLocation = glGetUniformLocation(programID, "normal");
	assert(iLocation != -1 && "If -1, could not find this uniform in the given shader.");
	glUniform1i(iLocation, 1);

	// bind the light
	glm::vec3 light(sin(glfwGetTime()), 1, cos(glfwGetTime()));
	iLocation = glGetUniformLocation(programID, "lightDir");
	assert(iLocation != -1 && "If -1, could not find this uniform in the given shader.");
	glUniform3f(iLocation, light.x, light.y, light.z);
	
	// draw the texture
	glBindVertexArray(m_VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
}

void TextureApp::GenerateTexture(GLuint & VBO, GLuint & IBO, GLuint & VAO)
{
	// set the data for the vertex float array
	TextureApp::Vertex vertexData[] =
	{
		{ -5, 0, 5, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1 },
		{ 5, 0, 5, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1 },
		{ 5, 0, -5, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0 },
		{ -5, 0, -5, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0 },
	};

	// set the data for the index integer array
	unsigned int indexData[] =
	{
		0, 1, 2,
		0, 2, 3,
	};

	// generate and bind the VAO to the m_VAO variable
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	// generate the VBO, bind it to the m_VBO variable & fill the buffer with data
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TextureApp::Vertex) * 4, vertexData, GL_STATIC_DRAW);

	// generate the IBO, bind it to the m_IBO variable & fill the buffer with data
	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 6, indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(TextureApp::Vertex), 0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(TextureApp::Vertex), ((char*)0) + 48);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(TextureApp::Vertex), ((char*)0) + 16);

	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(TextureApp::Vertex), ((char*)0) + 32);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
