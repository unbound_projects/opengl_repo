#pragma once
#include "../GameObject Class/BaseObject.h"
#include "../Program Helper Class/ProgramHelper.h"
#include <gl_core_4_4.h>
#include <glm\mat4x4.hpp>

class TextureApp : public BaseObject
{
public:
	TextureApp();
	~TextureApp();

	struct Vertex
	{
		float x, y, z, w;
		float nx, ny, nz, nw;
		float tx, ty, tz, tw;
		float s, t;
	};

	// Base Object Functions
	bool					Initialise();
	void					DeInitialise();
	bool					Update(float deltaTime);
	void					Draw(glm::mat4 projectionView);

	// Texture Rendering Functions
	void					GenerateTexture(GLuint& VBO, GLuint& IBO, GLuint& VAO);

private:
	// Buffers
	GLuint					m_VBO;
	GLuint					m_VAO;
	GLuint					m_IBO;

	GLuint					m_texture;
	ProgramHelper			m_program;
	GLuint					m_normalmap;
};