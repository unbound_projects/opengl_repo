#pragma once
#include "../GameObject Class/BaseObject.h"
#include "../Program Helper Class/ProgramHelper.h"
#include "gl_core_4_4.h"

class ShaderApplication : public BaseObject
{
public:
	ShaderApplication();
	~ShaderApplication();

	bool				Initialise() override;
	void				DeInitialise() override;
	bool				Update(float deltaTime) override;
	void				Draw(glm::mat4x4 projectionView) override;
	
	int					rows = 21;
	int					columns = 21;

	ProgramHelper		m_program;
	GLint				heightScale = 0;
private:
	// Buffers
	GLuint				m_VBO;
	GLuint				m_VAO;
	GLuint				m_IBO;

	GLint				time = 0;
};