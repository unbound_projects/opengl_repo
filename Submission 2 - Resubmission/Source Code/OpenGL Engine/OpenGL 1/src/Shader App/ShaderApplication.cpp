#include "ShaderApplication.h"
#include "../GeometryHelper.h"
#include <glm\gtc\type_ptr.hpp>
#include <glfw3.h>
#include <iostream>

ShaderApplication::ShaderApplication()
{
}


ShaderApplication::~ShaderApplication()
{
}

bool ShaderApplication::Initialise()
{
	const char* vsSource = "#version 410\n \
							layout(location=0) in vec4 Position; \
							layout(location=1) in vec4 Colour; \
							out vec4 vColour; \
							uniform mat4 ProjectionView; \
							uniform float time; \
							uniform float heightScale; \
							void main() \
							{ \
								vColour = Colour; \
								vec4 P = Position; \
								P.y += sin( time + Position.x ) * heightScale; \
								gl_Position = ProjectionView * P; }";

	const char* fsSource = "#version 410\n \
							in vec4 vColour; \
							out vec4 FragColour; \
							void main() { FragColour = vColour; }";

	m_program.Create(vsSource, fsSource);

	GeometryHelper::GenerateGrid(rows, columns, m_VAO, m_VBO, m_IBO);

	return true;
}

void ShaderApplication::DeInitialise()
{

}

bool ShaderApplication::Update(float deltaTime)
{
	return true;
}

void ShaderApplication::Draw(glm::mat4x4 projectionView)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLuint programID = m_program.GetID();

	glUseProgram(programID);

	unsigned int projectionViewUniform = glGetUniformLocation(programID, "ProjectionView");	
	glUniformMatrix4fv(projectionViewUniform, 1, false, glm::value_ptr(projectionView));

	time = glGetUniformLocation(programID, "time");
	assert(time != -1);
	glUniformMatrix4fv(time, 1, false, glm::value_ptr(projectionView));

	heightScale = glGetUniformLocation(programID, "heightScale");
	assert(heightScale != -1);
	glUniformMatrix4fv(heightScale, 1, false, glm::value_ptr(projectionView));

	glBindVertexArray(m_VAO);

	unsigned int indexCount = (rows - 1) * (columns - 1) * 6;
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}