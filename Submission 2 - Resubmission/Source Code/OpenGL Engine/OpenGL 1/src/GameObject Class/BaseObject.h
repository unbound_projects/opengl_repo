#pragma once
#include "glm\mat4x4.hpp"
#include "../Collision and Culling Class/SphereCollider.h"

class FlyCamera;

class BaseObject
{
public:
	BaseObject();
	~BaseObject();

	virtual bool Initialise() = 0;
	virtual void DeInitialise() = 0;
	virtual bool Update(float deltaTime) = 0;
	virtual void Draw(glm::mat4 projectionView) = 0;

	void SetCamera(FlyCamera* camera);

	FlyCamera* m_camera;
	SphereCollider* collider;
	glm::mat4 m_globalTransform = glm::mat4(1);
};