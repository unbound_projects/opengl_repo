#pragma once
#include "../GameObject Class/BaseObject.h"
#include "tiny_obj_loader.h"
#include "../Program Helper Class/ProgramHelper.h"
#include <vector>

class OBJApplication : public BaseObject
{
public:
	OBJApplication();
	~OBJApplication();

	struct OpenGLInfo
	{
		unsigned int m_VAO;
		unsigned int m_VBO;
		unsigned int m_IBO;
		unsigned int m_index_count;
	};

	bool			Initialise();
	void			DeInitialise();
	bool			Update(float deltaTime);
	void			Draw(glm::mat4x4 projectionView);

	void			CreateOpenGLBuffers(std::vector<tinyobj::shape_t>& shapes);	

	std::vector<OpenGLInfo>			m_gl_info;

private:
	GLuint							m_localProgram;
	std::vector<tinyobj::shape_t>	shapes;
	std::vector<tinyobj::material_t> materials;
	std::string						err;
};