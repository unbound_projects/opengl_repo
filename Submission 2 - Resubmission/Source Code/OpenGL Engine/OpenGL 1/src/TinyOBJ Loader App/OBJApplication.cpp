#include "OBJApplication.h"
#include "../Helper.h"
#include "tiny_obj_loader.h"
#include "glm/gtc/type_ptr.hpp"

OBJApplication::OBJApplication()
{
}


OBJApplication::~OBJApplication()
{
}

bool OBJApplication::Initialise()
{
	CreateOpenGLBuffers(shapes);
	return true;
}

void OBJApplication::DeInitialise()
{
}

bool OBJApplication::Update(float deltaTime)
{
	return true;
}

void OBJApplication::Draw(glm::mat4x4 projectionView)
{
	glUseProgram(m_localProgram);

	// Search for the projectionView uniform matrix, fill it with data.
	int iLocation = glGetUniformLocation(m_localProgram, "ProjectionView");
	assert(iLocation != -1);
	glUniformMatrix4fv(iLocation, 1, GL_FALSE, glm::value_ptr(projectionView));

	// Go through the list of obj files, bind their VAO's and draw
	for (unsigned int i = 0; i < m_gl_info.size(); i++)
	{
		glBindVertexArray(m_gl_info[i].m_VAO);
		glDrawElements(GL_TRIANGLES, m_gl_info[i].m_index_count, GL_UNSIGNED_INT, 0);
	}
}

void OBJApplication::CreateOpenGLBuffers(std::vector<tinyobj::shape_t>& shapes)
{
	bool successful = tinyobj::LoadObj(shapes, materials, err, "../data/models/Stanford/sphere.obj");

	// SETTING UP THE VERTEX SHADER **************************
	const char* vsSource =
		R"(
			#version 410
			layout(location = 0) in vec4 Position;
			layout(location = 1) in vec2 textureCoord;
			out vec2 vTexCoord;
			uniform mat4 ProjectionView;
			
			void main() 
			{
				vTexCoord = textureCoord;
				gl_Position = ProjectionView * Position;
			};
		)";

	// SETTING UP THE FRAGMENT SHADER **************************
	const char* fsSource = 
		R"(
			#version 410
			in vec2 vTexCoord; 
			out vec4 FragColour; 
			uniform sampler2D diffuse; 
			
			void main() 
			{ 
				FragColour = texture(diffuse, vTexCoord); 
			};
		)";

	// defensive programming check for shader linking
	int shaderLinkSuccess = GL_FALSE;

	// creating the vertex shader, give it a source and compile.
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, (const char**)&vsSource, 0);
	glCompileShader(vertexShader);

	// assert to check the compilation of the vertex shader
	bool vCompileTest = Helper::CheckCompileStatus(vertexShader);
	assert(vCompileTest && "Vertex Shader did not compile properly.");

	// creating the fragment shader, give it a source and compile.
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, (const char**)&fsSource, 0);
	glCompileShader(fragmentShader);

	// assert to check the compilation of the fragment shader
	bool fCompileTest = Helper::CheckCompileStatus(vertexShader);
	assert(fCompileTest && "Fragment Shader did not compile properly.");

	// create the program and attach the shaders to the program
	m_localProgram = glCreateProgram();
	glAttachShader(m_localProgram, vertexShader);
	glAttachShader(m_localProgram, fragmentShader);

	// link the program & check to see if the link was successful
	glLinkProgram(m_localProgram);
	glGetProgramiv(m_localProgram, GL_LINK_STATUS, &shaderLinkSuccess);

	// assert to check the linking of the program
	bool linkTest = Helper::CheckLinkStatus(m_localProgram);
	assert(linkTest && "Shaders did not link to program properly.");

	m_gl_info.resize(shapes.size());

	for (unsigned int mesh_index = 0; mesh_index < shapes.size(); mesh_index++)
	{		
		// generate the buffers for each mesh
		glGenVertexArrays(1, &m_gl_info[mesh_index].m_VAO);
		glGenBuffers(1, &m_gl_info[mesh_index].m_VBO);
		glGenBuffers(1, &m_gl_info[mesh_index].m_IBO);
		glBindVertexArray(m_gl_info[mesh_index].m_VAO);

		unsigned int float_count = shapes[mesh_index].mesh.positions.size();

		// increase the total count by the amount of normals and textures in the mesh
		float_count += shapes[mesh_index].mesh.normals.size();
		float_count += shapes[mesh_index].mesh.texcoords.size();

		// create a variable to store vertex data in & reserve the amount of space for size of float_count
		std::vector<float> vertex_data;
		vertex_data.reserve(float_count);

		// insert the data for each mesh's positions into the vertex data
		vertex_data.insert(vertex_data.end(),
			shapes[mesh_index].mesh.positions.begin(),
			shapes[mesh_index].mesh.positions.end());

		// insert the data for each mesh's normals into the vertex data
		vertex_data.insert(vertex_data.end(),
			shapes[mesh_index].mesh.normals.begin(),
			shapes[mesh_index].mesh.normals.end());

		// size up the index count based off the indices
		m_gl_info[mesh_index].m_index_count = shapes[mesh_index].mesh.indices.size();

		glBindBuffer(GL_ARRAY_BUFFER, m_gl_info[mesh_index].m_VBO);
		glBufferData(GL_ARRAY_BUFFER, vertex_data.size() * sizeof(float), vertex_data.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_gl_info[mesh_index].m_IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, shapes[mesh_index].mesh.indices.size() * sizeof(unsigned int), shapes[mesh_index].mesh.indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 0, (void*)(sizeof(float)* shapes[mesh_index].mesh.positions.size()));

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
}
