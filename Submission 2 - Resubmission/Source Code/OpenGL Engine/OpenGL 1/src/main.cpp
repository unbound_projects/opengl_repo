#include <gl_core_4_4.h>
#include "Flying Camera Class\FlyCamera.h"
#include "Base Classes\BaseApplication.h"
#include "Solar System Application\SolarSystemApp.h"
#include "Shader App\ShaderApplication.h"
#include "Texture App\TextureApp.h"
#include "FBX Loader App\FBXApplication.h"
#include "TinyOBJ Loader App\OBJApplication.h"
#include "FBX Animator Handling App\FBXAnimatorApp.h"
#include "Procedural Generation\GenerationApp.h"

int main()
{
	/* GENERATION APP ALWAYS NEEDS TO BE LAST */

	BaseApplication* app = new BaseApplication();	
	app->AddObject(new ParticleApp());
	app->AddObject(new FBXApplication("../data/models/Stanford/soulspear/soulspear.fbx", glm::vec3(0, 50, 0), 1));
	app->AddObject(new FBXApplication("../data/models/Stanford/soulspear/soulspear.fbx", glm::vec3(0, 25, 0), 2));
	app->AddObject(new FBXApplication("../data/models/Stanford/sphere.fbx", glm::vec3(500, 0, 500), 3));
	app->AddObject(new GenerationApp());

	app->Run();
	return 0;
}