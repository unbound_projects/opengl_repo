#include "ParticleApp.h"
#include "Emitter.h"
#include "../Flying Camera Class/FlyCamera.h"
#include <glm\glm.hpp>
#include <glfw3.h>
#include <glm\gtc\type_ptr.hpp>

bool ParticleApp::Initialise()
{
	m_emitter = new ParticleEmitter();
	m_emitter->Initialise(1000, 50000, 1.0f, 100.0f, 5, 50, 1, 0.1f, glm::vec4(1, 1, 1, 1), glm::vec4(1, 1, 1, 1), glm::vec3(500, 2200, 500));

	SetCamera(m_camera);

	return true;
}
void ParticleApp::DeInitialise()
{
	delete m_emitter;
}
bool ParticleApp::Update(float deltaTime)
{
	m_emitter->Update(deltaTime, m_camera->GetWorldTransform());
	return true;
}
void ParticleApp::Draw(glm::mat4x4 projectionMatrix)
{
	m_emitter->Draw(projectionMatrix);
}