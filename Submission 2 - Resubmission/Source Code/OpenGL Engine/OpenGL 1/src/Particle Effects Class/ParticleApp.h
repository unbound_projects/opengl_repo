#pragma once
#include "../GameObject Class/BaseObject.h"
#include "../Program Helper Class/ProgramHelper.h"
#include <gl_core_4_4.h>
#include <glm\vec3.hpp>

class ParticleEmitter;

class ParticleApp : public BaseObject
{
public:
	bool			Initialise();
	void			DeInitialise();
	bool			Update(float deltaTime);
	void			Draw(glm::mat4x4 projectionMatrix);

	ParticleEmitter* m_emitter;
};