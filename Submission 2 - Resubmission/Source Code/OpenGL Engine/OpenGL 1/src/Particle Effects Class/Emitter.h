#pragma once
#include <glm\glm.hpp>
#include <gl_core_4_4.h>
#include "../Program Helper Class/ProgramHelper.h"

struct Particle
{
	glm::vec3		position;
	glm::vec3		velocity;
	glm::vec4		colour;
	float			size;
	float			lifetime;
	float			lifespan;
};

// Struct to create our particle vertex's
struct ParticleVertex
{
	glm::vec4		position;
	glm::vec4		colour;
};

// Inside-class to control our particle emitter
class ParticleEmitter
{
public:
	ParticleEmitter() : m_particles(nullptr), m_firstDead(0), m_maxParticles(0), m_position(0, 0, 0), m_VAO(0), m_VBO(0), m_IBO(0), m_vertexData(nullptr) {};
	virtual ~ParticleEmitter();

	void			Initialise(GLuint a_maxParticles, GLuint a_emitRate, GLfloat a_lifetimeMin, GLfloat a_lifetimeMax,
		GLfloat a_velocityMin, GLfloat a_velocityMax, GLfloat a_startSize, GLfloat a_endSize,
		const glm::vec4& a_startColour, const glm::vec4& a_endColour, glm::vec3 a_position);

	void			Emit();
	void			Update(float a_deltaTime, const glm::mat4& a_cameraTransform);
	void			Draw(glm::mat4x4 projectionMatrix);

	// Cleanup Functions
	void			GenerateShaders();
	void			GenerateAndBindBuffers(GLuint* indexData);

protected:
	Particle*		m_particles;
	GLuint			m_firstDead;
	GLuint			m_maxParticles;

	glm::vec3		m_position;

	// EMITTER COLOUR
	glm::vec4		m_startColour;
	glm::vec4		m_endColour;

	// EMITTER TIMER & RATE OF CREATION
	float			m_emitTimer = 0;
	float			m_emitRate;

	// EMITTER LIFESPAN
	float			m_lifespanMin;
	float			m_lifespanMax;

	// EMITTER VELOCITY
	float			m_velocityMin;
	float			m_velocityMax;

	// EMITTER SIZE
	float			m_startSize;
	float			m_endSize;

	// BUFFERS & VERTEX DATA
	GLuint			m_VAO, m_VBO, m_IBO;
	ParticleVertex* m_vertexData;

	ProgramHelper m_particleapp_program;
};