#pragma once
#include "gl_core_4_4.h"

#define printOpenGLError() printOglError(__FILE__, __LINE__)

class Helper
{
public:
	Helper();
	~Helper();

	static bool			CheckLinkStatus(GLuint programID);
	static bool			CheckCompileStatus(GLuint shaderID);

	static int			printOglError(char *file, int line);
};

