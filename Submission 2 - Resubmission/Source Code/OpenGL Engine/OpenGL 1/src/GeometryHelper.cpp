#include "GeometryHelper.h"
#include <glm\vec3.hpp>
#include <glm\vec4.hpp>
#include <cmath>

using glm::vec2;
using glm::vec3;
using glm::vec4;

GeometryHelper::GeometryHelper()
{
}

GeometryHelper::~GeometryHelper()
{
}

void GeometryHelper::GenerateGrid(GLuint rows, GLuint columns, GLuint & VAO, GLuint & VBO, GLuint & IBO)
{
	// create array of vertices to pass to the GPU
	Vertex* verts = new Vertex[rows * columns];

	for (int iter_rows = 0; iter_rows < rows; iter_rows++)
	{
		for (int iter_columns = 0; iter_columns < columns; iter_columns++)
		{
			int vertsIndex = iter_rows * columns + iter_columns;
			verts[vertsIndex].position = vec4((float)iter_columns, 0, (float)iter_rows, 1);

			verts[vertsIndex].texCoord = vec2(verts[vertsIndex].position.x / 75, verts[vertsIndex].position.z / 75);

			// create some arbitrary colour based off something that might not be related to tiling a texture
			vec3 colour = vec3(sinf((iter_columns / (float)(columns - 1)) * (iter_rows / (float)(rows - 1))));

			verts[vertsIndex].colour = vec4(colour, 1);

		}
	}

	unsigned int* indices = new unsigned int[(rows - 1) * (columns - 1) * 6];

	unsigned int index = 0;
	for (unsigned int iter_rows = 0; iter_rows < (rows - 1); ++iter_rows)
	{
		for (unsigned int iter_columns = 0; iter_columns < (columns - 1); ++iter_columns)
		{
			//triangle one
			indices[index++] = iter_rows * columns + iter_columns;
			indices[index++] = (iter_rows + 1) * columns + iter_columns;
			indices[index++] = (iter_rows + 1) * columns + (iter_columns + 1);

			//triangle two
			indices[index++] = iter_rows * columns + iter_columns;
			indices[index++] = (iter_rows + 1) * columns + (iter_columns + 1);
			indices[index++] = iter_rows * columns + (iter_columns + 1);
		}
	}

	// generate the VBO [vertex buffer object]

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, (rows * columns) * sizeof(Vertex), verts, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4)));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoord));

	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (rows - 1) * (columns - 1) * 6 * sizeof(unsigned int), indices, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] indices;
	delete[] verts;
}