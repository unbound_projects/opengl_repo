#include "ProgramHelper.h"
#include "../Helper.h"
#include <assert.h>
#include <stdio.h>

ProgramHelper::ProgramHelper()
{
}

ProgramHelper::~ProgramHelper()
{
}

bool ProgramHelper::Create(const char * vertexShaderSource, const char * fragmentShaderSource)
{
	// setting up a defensive programming check to make sure the shaders link
	int shaderLinkSuccess = GL_FALSE;

	// creating the vertex shader, giving it a source and compiling
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShaderID, 1, (const char**)&vertexShaderSource, 0);
	glCompileShader(vertexShaderID);

	// assert to check the compilation of the vertex shader
	bool vTest = Helper::CheckCompileStatus(vertexShaderID);
	if (!vTest)
	{
		assert(vTest && "Vertex Shader did not compile properly.");
		return false;
	}

	// creating the fragment shader, giving it a source and compiling
	GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderID, 1, (const char**)&fragmentShaderSource, 0);
	glCompileShader(fragmentShaderID);

	// assert to check the compilation of the fragment shader
	bool fTest = Helper::CheckCompileStatus(fragmentShaderID);
	if (!fTest) {
		assert(fTest && "Fragment Shader did not compile properly.");
		return false;
	}

	// create the program and attach the shaders
	m_program = glCreateProgram();
	glAttachShader(m_program, vertexShaderID);
	glAttachShader(m_program, fragmentShaderID);

	// link the program & check if the link worked
	glLinkProgram(m_program);
	glGetProgramiv(m_program, GL_LINK_STATUS, &shaderLinkSuccess);

	// assert to check if the linking shaders was successful, if not - crash
	bool linkTest = Helper::CheckLinkStatus(m_program);
	if (!linkTest)
	{
		GLint infoLogLength;
		glGetShaderiv(m_program, GL_INFO_LOG_LENGTH, &infoLogLength);

		char* info = new char[infoLogLength];
		glGetShaderInfoLog(m_program, infoLogLength, NULL, info);
		printf_s("ShaderProgram Error Log:%s", info);
		assert(Helper::CheckLinkStatus(m_program) && "Shaders did not link to program.");

		delete[] info;

		return false;
	}

	// shaders are not embedded into the program and can be deleted
	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

	glUseProgram(m_program);
	return true;
}