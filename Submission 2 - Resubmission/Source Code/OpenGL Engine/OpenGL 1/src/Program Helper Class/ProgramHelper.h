#pragma once
#include "gl_core_4_4.h"

class ProgramHelper
{
public:
	ProgramHelper();
	~ProgramHelper();

	bool Create(const char* vertexShaderSource, const char* fragmentShaderSource);
	GLuint GetID() const { return m_program; }

private:
	GLuint m_program;
};