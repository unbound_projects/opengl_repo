#pragma once
#include "..\Base Classes\BaseCamera.h"
#include "glm\glm.hpp"
#include "glfw3.h"

using glm::vec3;

class FlyCamera : public BaseCamera
{
public:
	FlyCamera();
	virtual ~FlyCamera();

	void			Update(float deltaTime);
	void			SetFlySpeed(float speed) { flySpeed = speed; }
	float			GetFlySpeed() { return flySpeed; }
	void			SetRotationSpeed(float value) { rotSpeed = value; }
	float			GetRotationSpeed() { return rotSpeed; }
	void			SetWindow(GLFWwindow* newWindow) { m_pWindow = newWindow; }

protected:
	void			HandleKeyboardInput(double deltaTime);
	void			HandleMouseInput(double deltaTime);
	void			CalculateRotation(double deltaTime, double xOffset, double yOffset);

	GLFWwindow*		m_pWindow;

	float			flySpeed;
	float			rotSpeed;
	bool			buttonClicked;
	double			cursorX, cursorY;
};