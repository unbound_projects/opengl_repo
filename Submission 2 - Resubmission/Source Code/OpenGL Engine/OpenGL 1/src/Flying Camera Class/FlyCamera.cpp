#include "FlyCamera.h"
#include "glm\glm.hpp"
#include "glm\ext.hpp"
#include "glm\gtx\transform.hpp"
#include <assert.h>

FlyCamera::FlyCamera()
{

}

FlyCamera::~FlyCamera()
{

}

void FlyCamera::Update(float deltaTime)
{
	// Check to make sure the window isn't nullptr
	assert(m_pWindow != nullptr);

	// Update the mouse and keyboard input
	HandleMouseInput(deltaTime);
	HandleKeyboardInput(deltaTime);	
}

void FlyCamera::HandleKeyboardInput(double deltaTime)
{
	mat4 newTrans = GetWorldTransform();

	vec3 rightMovement = vec3(newTrans[0].x, newTrans[0].y, newTrans[0].z);
	vec3 upMovement = vec3(newTrans[1].x, newTrans[1].y, newTrans[1].z);
	vec3 forwardMovement = vec3(newTrans[2].x, newTrans[2].y, newTrans[2].z);
	vec3 moveDirection(0.0f);

	// Impend movement based on what key is pressed
	if (glfwGetKey (m_pWindow, GLFW_KEY_W) == GLFW_PRESS)
	{
		moveDirection -= forwardMovement;
	}

	if (glfwGetKey(m_pWindow, GLFW_KEY_S) == GLFW_PRESS)
	{
		moveDirection += forwardMovement;
	}

	if (glfwGetKey(m_pWindow, GLFW_KEY_A) == GLFW_PRESS)
	{
		moveDirection -= rightMovement;
	}

	if (glfwGetKey(m_pWindow, GLFW_KEY_D) == GLFW_PRESS)
	{
		moveDirection += rightMovement;
	}

	if (glfwGetKey(m_pWindow, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		moveDirection += upMovement;
	}

	if (glfwGetKey(m_pWindow, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
	{
		moveDirection -= upMovement;
	}

	// Move so long as we can move, update the position based on the move direction
	float fLength = glm::length(moveDirection);
	if (fLength > 0.01f)
	{
		moveDirection = ((float)deltaTime * flySpeed) * glm::normalize(moveDirection);
		SetPosition(GetPosition() + moveDirection);
	}
}

void FlyCamera::HandleMouseInput(double deltaTime)
{	
	if (glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS)
	{
		if (buttonClicked == false)
		{
			int width, height;
			// get the frame buffer size from the window, store in width/height
			glfwGetFramebufferSize(m_pWindow, &width, &height);

			cursorX = width / 2.0;
			cursorY = height / 2.0;

			// Set the cursor position in the window to half the width and height
			glfwSetCursorPos(m_pWindow, width / 2, height / 2);
			buttonClicked = true;
		}

		else
		{
			double mouseX, mouseY;
			// Get the cursor pos
			glfwGetCursorPos(m_pWindow, &mouseX, &mouseY);

			double xOffset = mouseX - cursorX;
			double yOffset = mouseY - cursorY;

			// Calculate the rotation based off the offsets of the mouse pos
			CalculateRotation(deltaTime, xOffset, yOffset);
		}

		int width, height;
		// Get the frame buffer size, set the cursor position
		glfwGetFramebufferSize(m_pWindow, &width, &height);
		glfwSetCursorPos(m_pWindow, width / 2, height / 2);
	}

	else
	{
		buttonClicked = false;
	}
}

void FlyCamera::CalculateRotation(double deltaTime, double xOffset, double yOffset)
{
	// If the 2 offsets passed in equal 0, stop
	if (xOffset == 0 && yOffset == 0) return;
	
	if (xOffset != 0.0)
	{
		// Set up the rotation matrix
		mat4 rotationMatrix = glm::rotate((float)(rotSpeed * deltaTime * -xOffset), glm::vec3(0, 1, 0));

		// Update the transform to use the rotation matrix
		SetTransform(GetWorldTransform() * rotationMatrix);
	}

	if (yOffset != 0.0)
	{
		// Same as above
		mat4 rotationMatrix = glm::rotate((float)(rotSpeed * deltaTime * -yOffset), glm::vec3(1, 0, 0));

		SetTransform(GetWorldTransform() * rotationMatrix);
	}

	// Store the old transform
	mat4 oldTransform = GetWorldTransform();

	mat4 newTransform;
	vec3 worldUp = vec3(0, 1, 0);

	// Store the old forward axis
	vec3 oldForward = vec3(oldTransform[2].x, oldTransform[2].y, oldTransform[2].z);

	newTransform[0] = glm::normalize(glm::vec4(glm::cross(worldUp, oldForward), 0));

	// Overwrite the old right axis with the new right axis
	vec3 newRight = vec3(newTransform[0].x, newTransform[0].y, newTransform[0].z);
	newTransform[1] = glm::normalize(glm::vec4(glm::cross(oldForward, newRight), 0));

	newTransform[2] = glm::normalize(oldTransform[2]);
	newTransform[3] = oldTransform[3];
	
	// Update the transform
	SetTransform(newTransform);
	
}