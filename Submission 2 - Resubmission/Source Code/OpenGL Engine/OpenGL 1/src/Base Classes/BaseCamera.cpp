#include "BaseCamera.h"

BaseCamera::BaseCamera()
{

}

BaseCamera::~BaseCamera()
{

}

void BaseCamera::SetPerspective(float fov, float aspect, float near, float far)
{
	// Set up the perspective of the camera using the FOV, aspect ratio, near view and far view
	projectionTransform = glm::perspective(fov, aspect, near, far);

	UpdateProjectionViewTransform();
}

void BaseCamera::SetLookAt(vec3 to, vec3 up)
{
	// Set up the view transform and the world transform, then update the projection view transform
	viewTransform = glm::lookAt(GetPosition(), to, up);
	worldTransform = glm::inverse(viewTransform);
	UpdateProjectionViewTransform();
}

void BaseCamera::SetPosition(vec3 position)
{
	// Set up the position of the camera to be the local world transform
	worldTransform[3] = vec4(position, 1);
	UpdateProjectionViewTransform();
}

void BaseCamera::UpdateProjectionViewTransform()
{
	// Make the view transform the inverse of the world transform
	viewTransform = glm::inverse(worldTransform);

	// The projection view transform is the projection transform multiplied by the view transform
	projectionViewTransform = projectionTransform * viewTransform;
}

void BaseCamera::SetTransform(mat4 newTrans)
{ 
	// Reset the world transform and update the projection view transform
	worldTransform = newTrans; 
	UpdateProjectionViewTransform();
}