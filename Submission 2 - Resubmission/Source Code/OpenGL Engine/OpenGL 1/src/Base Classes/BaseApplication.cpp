#include "BaseApplication.h"
#include "..\Procedural Generation\GenerationApp.h"
#include "..\Solar System Application\Planet.h"
#include "..\Collision and Culling Class\SphereCollider.h"
#include "..\Collision and Culling Class\FrustumCollision.h"
#include <vector>
#include <assert.h>
#include <glm\vec3.hpp>

/* STATIC VARIABLES - FOR ADDING TO GUI IN OTHER APPLICATIONS */

// FBX
glm::vec3 BaseApplication::gui_FBXAmbientColour = glm::vec3(1, 1, 1);
glm::vec3 BaseApplication::gui_FBXDiffuseColour = glm::vec3(1, 1, 1);
glm::vec3 BaseApplication::gui_FBXSpecularColour = glm::vec3(1, 0, 0);
glm::vec3 BaseApplication::gui_FBXLightDirection = glm::vec3(0, 0, -1);
glm::vec3 BaseApplication::gui_FBX1Position = glm::vec3(0, 50, 0);
glm::vec3 BaseApplication::gui_FBX2Position = glm::vec3(0, 25, 0);
float BaseApplication::gui_FBXSpecularPower = 32.0f;
float BaseApplication::gui_FBXScaleRate = 1.0f;


// PROCEDURAL GEN.
glm::vec3 BaseApplication::gui_GenAmbientColour = glm::vec3(1, 1, 1);
glm::vec3 BaseApplication::gui_GenLightDirection = glm::vec3(0, -1, 0);
glm::vec3 BaseApplication::gui_GenDiffuseColour = glm::vec3(1, 1, 1);
void TW_CALL GeneratePerlinNoise(void* data);

// WATER
float BaseApplication::gui_WaterHeight = 47.0f;
float BaseApplication::gui_WaterOpacity = 0.3f;
float BaseApplication::gui_WaterHeightScale = 0.3f;

// PARTICLE EFFECTS
glm::vec4 BaseApplication::gui_particleStartColour = glm::vec4(1, 1, 1, 1);
glm::vec4 BaseApplication::gui_particleEndColour = glm::vec4(1, 1, 1, 1);
float BaseApplication::gui_particleEmitRate = 0.0001f;
float BaseApplication::gui_particleLifetimeMin = 1.0f;
float BaseApplication::gui_particleLifetimeMax = 50.0f;
float BaseApplication::gui_particleStartSize = 4.0f;
float BaseApplication::gui_particleEndSize = 1.f;
float BaseApplication::gui_particleMinVelocity = 10.0f;
float BaseApplication::gui_particleMaxVelocity = 50.0f;

// SKYBOX
float BaseApplication::gui_SkyboxScaleRate = 2000.0f;

bool BaseApplication::fbxScaleHasChanged = false;

BaseApplication::BaseApplication()
{
	deltaTime = 0;
}

BaseApplication::~BaseApplication()
{
	gui_FBXAmbientColour = vec3(0, 0, 0);
	gui_FBXDiffuseColour = vec3(1, 1, 1);
	gui_FBXSpecularColour = vec3(0, 0, 0);
	gui_FBXLightDirection = vec3(0, 0, -1);
	gui_GenAmbientColour = vec3(1, 1, 1);
	gui_GenLightDirection = vec3(0, -1, 0); 
	gui_GenDiffuseColour = vec3(1, 1, 1);
	gui_FBXSpecularPower = 32.0f;
	gui_WaterHeight = 47.0f;
	gui_WaterOpacity = 0.3f;
	gui_WaterHeightScale = 0.3f;
	gui_particleEmitRate = 5.0f;
	gui_particleLifetimeMin = 1.0f;
	gui_particleLifetimeMax = 100.0f;
	gui_particleStartSize = 1.0f;
	gui_particleEndSize = 0.1f;
}

void BaseApplication::Run()
{
	int iResult = InitOpenGL();
	assert(iResult == 0);

	// IF initialise works & applications continue to update
	if (Initialise() == true)
	{
		while (Update() == true)
		{		
			// update the engine & constantly draw things (in case of updates)
			UpdateEngine();
			PreDrawEngine();			
			Draw();
			PostDrawEngine();
		}
		// if updates hits false, we're ending the program
		ShutdownEngine();
	}
}

bool BaseApplication::Initialise()
{
	// for each base object, go through and initialise. If initialise fails, stop program
	for (BaseObject* object : m_Objects)
	{
		if (!object->Initialise())
		{
			return false;
		}
	}
	return true;
}

void BaseApplication::DeInitialise()
{
	// once the application is called to stop, deinitialise all objects to remove memory leaks
	for (BaseObject* object : m_Objects)
	{
		object->DeInitialise();
	}

	delete[] flyCam;
}

bool BaseApplication::Update()
{
	// update all the base objects with deltatime
	for (BaseObject* object : m_Objects)
	{
		object->Update(deltaTime);
	}

	glClearColor(BaseApplication::m_clearColour.x, m_clearColour.y, m_clearColour.z, 0);

	flyCam->SetFlySpeed(gui_camSpeed);

	return !glfwGetKey(m_window, GLFW_KEY_ESCAPE);
}

void BaseApplication::Draw()
{
	int invisObj = 0;
	// draw all the objects, update frustum culling
	for (BaseObject* object : m_Objects)
	{
		bool visible = IsVisible(object);
		
		if (visible)
		{
			if (object->collider != nullptr)
			{
				glm::vec3 spherePos = object->collider->centre;
				spherePos += glm::vec3( object->m_globalTransform[3] );

				Gizmos::addSphere(spherePos, object->collider->radius, 8, 8, glm::vec4(1, 0, 1, 0));
			}
			object->Draw(flyCam->GetProjectionViewTransform());
		}
		else
		invisObj++;
	}
	printf("%i : Number of Objects not visible, so not rendering.\n", invisObj);
}

void BaseApplication::AddObject(BaseObject* objToAdd)
{
	// add a new object to the object list
	objToAdd->SetCamera(flyCam);
	m_Objects.push_back(objToAdd);
}

bool BaseApplication::IsVisible(BaseObject* object)
{
	SphereCollider* s_collider;
	s_collider = object->collider;

	if (s_collider == nullptr)
		return true;

	FrustumCollision frustum;

	glm::vec4 planes[6];
	frustum.GetFrustumPlanes(flyCam->GetProjectionViewTransform(), planes);
	
	glm::vec3 collidePosition = glm::vec3(object->m_globalTransform * glm::vec4(s_collider->centre, 1));

	for (int i = 0; i < 6; i++)
	{
		// find the dot product between my colliders position and the frustum planes
		float d = glm::dot(glm::vec3(planes[i]), collidePosition) + planes[i].w;

		// if the dot product of my collider is outside my frustum planes, it's not visible
		if (d < -s_collider->radius)
		{
			return false;
		}
	}
	return true;
}

int BaseApplication::InitOpenGL()
{
	//initialise a window, return -1 if this fails.
	if (glfwInit() == false)
		return -1;

	glfwWindowHint(GLFW_ALPHA_BITS, 8);
	m_window = glfwCreateWindow(1080, 720, "New OpenGL Window", nullptr, nullptr);

	//create a window and if this fails return -2
	if (m_window == nullptr)
	{
		glfwTerminate();
		return -2;
	}

	glfwMakeContextCurrent(m_window);

	//Load Functions for OpenGL Loader
	if (ogl_LoadFunctions() == ogl_LOAD_FAILED) 
	{
		glfwDestroyWindow(m_window);
		glfwTerminate();
		return -3;
	}

	Gizmos::create();

	//create a camera & set all its values
	flyCam->SetWindow(m_window);
	flyCam->SetPosition(vec3(-150, 500, 250));
	flyCam->SetRotationSpeed(0.070f);
	flyCam->SetLookAt(vec3(500, 150, 500), vec3(0, 5, 0));
	flyCam->SetPerspective(glm::pi<float>() * 0.25f, 16 / 9.f, 0.5f, 10000.f);
	//flyCam->SetFlySpeed(250.0f);

	// Set the clear colour & enable the gl depth buffer
	glClearColor(m_clearColour.x, m_clearColour.y, m_clearColour.z, 0);
	glEnable(GL_DEPTH_TEST);

	TwInit(TW_OPENGL_CORE, nullptr);
	TwWindowSize(1280, 720);

	// Set up the GLFW callbacks
	glfwSetMouseButtonCallback(m_window, OnMouseButton);
	glfwSetCursorPosCallback(m_window, OnMousePosition); 
	glfwSetScrollCallback(m_window, OnMouseScroll);
	glfwSetKeyCallback(m_window, OnKey);
	glfwSetCharCallback(m_window, OnChar);
	glfwSetWindowSizeCallback(m_window, OnWindowResize);
	
	// Having a base GUI in the class means that every object is able to access it without having multiple GUIs
	m_guiBar = TwNewBar("Editor Toolbar");

	// PERLIN NOISE GUI VARIABLES
	TwAddVarRW(m_guiBar, "Perlin Light Direction", TW_TYPE_DIR3F, &gui_GenLightDirection[0], "group=Perlin");
	TwAddVarRW(m_guiBar, "Perlin Ambient Colour", TW_TYPE_COLOR3F, &gui_GenAmbientColour[0], "group=Perlin");
	TwAddVarRW(m_guiBar, "Perlin Diffuse Colour", TW_TYPE_COLOR3F, &gui_GenDiffuseColour[0], "group=Perlin");

	TwAddButton(m_guiBar, "Generate Perlin Noise", GeneratePerlinNoise, m_Objects.back(), "group=Perlin");

	// WATER GRID GUI VARIABLES
	TwAddVarRW(m_guiBar, "Water Opacity", TW_TYPE_FLOAT, &gui_WaterOpacity, "group=Water min=0 max=1 step=0.1");
	TwAddVarRW(m_guiBar, "Water Level", TW_TYPE_FLOAT, &gui_WaterHeight, "group=Water ");
	TwAddVarRW(m_guiBar, "Water Wave Scale", TW_TYPE_FLOAT, &gui_WaterHeightScale, "group=Water min=0 max=3 step=0.1");

	// CAMERA VARIABLES
	TwAddVarRW(m_guiBar, "Camera Speed", TW_TYPE_FLOAT, &gui_camSpeed, "group=Camera min=0 max=500");

	// PARTICLE APPLICATION VARIABLES
	TwAddVarRW(m_guiBar, "Start Colour", TW_TYPE_COLOR3F, &gui_particleStartColour[0], "group=Particles");
	TwAddVarRW(m_guiBar, "End Colour", TW_TYPE_COLOR3F, &gui_particleEndColour[0], "group=Particles");
	TwAddVarRW(m_guiBar, "Min. Lifetime", TW_TYPE_FLOAT, &gui_particleLifetimeMin, "group=Particles min=1");
	TwAddVarRW(m_guiBar, "Max Lifetime", TW_TYPE_FLOAT, &gui_particleLifetimeMax, "group=Particles min=100");
	TwAddVarRW(m_guiBar, "Start Size", TW_TYPE_FLOAT, &gui_particleStartSize, "group=Particles min=1 max=10");
	TwAddVarRW(m_guiBar, "End Size", TW_TYPE_FLOAT, &gui_particleEndSize, "group=Particles min=0.1 max=1 step=0.1");
	TwAddVarRW(m_guiBar, "Emit Rate", TW_TYPE_FLOAT, &gui_particleEmitRate, "group=Particles min=0.0001 max=2 step=0.001");
	TwAddVarRW(m_guiBar, "Min. Velocity", TW_TYPE_FLOAT, &gui_particleMinVelocity, "group=Particles min=0.1 max=50");
	TwAddVarRW(m_guiBar, "Max. Velocity", TW_TYPE_FLOAT, &gui_particleMaxVelocity, "group=Particles min=50 max=500");

	// FBX GUI VARIABLES
	TwAddVarRW(m_guiBar, "Ambient Colour", TW_TYPE_COLOR3F, &gui_FBXAmbientColour[0], "group=FBX");
	TwAddVarRW(m_guiBar, "Diffuse Colour", TW_TYPE_COLOR3F, &gui_FBXDiffuseColour[0], "group=FBX");
	TwAddVarRW(m_guiBar, "Specular Colour", TW_TYPE_COLOR3F, &gui_FBXSpecularColour[0], "group=FBX");
	TwAddVarRW(m_guiBar, "Specular Power", TW_TYPE_FLOAT, &gui_FBXSpecularPower, "group=FBX min=0 max=1000");
	TwAddVarRW(m_guiBar, "Light Direction", TW_TYPE_DIR3F, &gui_FBXLightDirection[0], "group=FBX");
	TwAddVarRW(m_guiBar, "Scale Rate", TW_TYPE_FLOAT, &gui_FBXScaleRate, "group=FBX min=1 max=15 step=0.1");
	TwAddVarRW(m_guiBar, "FBX 1 Position", TW_TYPE_DIR3F, &gui_FBX1Position[0], "group=FBX step=1");
	TwAddVarRW(m_guiBar, "FBX 2 Position", TW_TYPE_DIR3F, &gui_FBX2Position[0], "group=FBX step=1");
	
	return 0;
}

void BaseApplication::PreDrawEngine()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	Gizmos::clear();
}

void BaseApplication::PostDrawEngine()
{	
	static bool _down;
	Gizmos::addTransform(glm::mat4(1));

	
	//draw with camera
	Gizmos::draw(flyCam->GetProjectionViewTransform());

	//Draw the GUI
	if (glfwGetKey(m_window, GLFW_KEY_O))
	{
		_down = true;
	}
	else if (glfwGetKey(m_window, GLFW_KEY_P))
	{
		_down = false;
	}

	if(_down)
		TwDraw();

	//do Input events
	glfwSwapBuffers(m_window);
	glfwPollEvents();
}

void BaseApplication::ShutdownEngine()
{
	//deinitialise all programs
	DeInitialise();

	//destroy gizmos
	Gizmos::destroy();

	//destroys pointer because of memory leaks
	glfwDestroyWindow(m_window);

	//Delete the GUI
	TwDeleteAllBars();

	//terminates a window
	TwTerminate();
	glfwTerminate();
}

void BaseApplication::UpdateEngine()
{
	// Update the deltatime, update the emitter and the camera
	static float pastFrame = 0;
	float currentFrame = (float)glfwGetTime();
	deltaTime = currentFrame - pastFrame;
	pastFrame = currentFrame;

	flyCam->Update(deltaTime);
}

void TW_CALL GeneratePerlinNoise(void* data)
{
	BaseObject* d = (BaseObject*)data;
	
	dynamic_cast<GenerationApp*>(d)->RegenerateNoise();
}