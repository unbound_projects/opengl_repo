#pragma once

#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>
#include <glm\ext.hpp>
#include <gl_core_4_4.h>
#include <glfw3.h>
#include "..\Flying Camera Class\FlyCamera.h"
#include "Gizmos.h"
#include "AntTweakBar.h"
#include "../GameObject Class/BaseObject.h"
#include "../Particle Effects Class/ParticleApp.h"
#include <vector>

using glm::mat4;
using glm::vec3;
using glm::vec4;

class Planet;
class ParticleEmitter;

class BaseApplication
{
public:
	BaseApplication();
	~BaseApplication();

	// Base Application Functions
	virtual bool		Initialise();
	virtual void		DeInitialise();
	virtual bool		Update();
	virtual void		Draw();

	// Other functions
	void				Run();
	void				AddObject(BaseObject* objToAdd);
	bool				IsVisible(BaseObject* object);

	// Setting up the shaders and program IDs
	GLuint				m_programID;

	float				deltaTime;

	#pragma region GUI Variables
	static glm::vec3	gui_FBXAmbientColour; 
	static glm::vec3	gui_FBXDiffuseColour;
	static glm::vec3	gui_FBXSpecularColour;
	static glm::vec3	gui_FBXLightDirection;
	static glm::vec3	gui_FBX1Position;
	static glm::vec3	gui_FBX2Position;
	static float		gui_FBXSpecularPower;
	static float		gui_FBXScaleRate; 
		
		
	static glm::vec3	gui_GenLightDirection;
	static glm::vec3	gui_GenAmbientColour;
	static glm::vec3	gui_GenDiffuseColour;
	static float		gui_GenMaxSandHeight;
	static float		gui_GenMinSandHeight;
	static float		gui_GenMaxGrassHeight;
	static float		gui_GenMinGrassHeight;
	static float		gui_GenMaxStoneHeight;
	static float		gui_GenMinStoneHeight;
	static float		gui_GenMaxSnowHeight;
	static float		gui_GenMinSnowHeight;

	static float		gui_WaterOpacity;
	static float		gui_WaterHeight;
	static float		gui_WaterHeightScale;

	static float		gui_particleEmitRate;
	static float		gui_particleLifetimeMin;
	static float		gui_particleLifetimeMax;
	static glm::vec4	gui_particleStartColour;
	static glm::vec4	gui_particleEndColour;
	static float		gui_particleStartSize;
	static float		gui_particleEndSize;
	static float		gui_particleMinVelocity;
	static float		gui_particleMaxVelocity;

	static float		gui_SkyboxScaleRate;

	float				gui_camSpeed = 250.0f;

	static bool			fbxScaleHasChanged;
	#pragma endregion

protected:
	GLFWwindow*			m_window;
	FlyCamera*			flyCam = new FlyCamera();

	// GUI Bar
	TwBar*				m_guiBar;

private:
	int					InitOpenGL();

	void				PreDrawEngine();
	void				PostDrawEngine();
	void				UpdateEngine();
	void				ShutdownEngine();

	std::vector<BaseObject*> m_Objects;

	glm::vec3			m_clearColour = glm::vec3(0.117f, 0.117f, 0.117f);
};

// Static functions for GLFW callback functions
static void OnMouseButton(GLFWwindow*, int b, int a, int m) { TwEventMouseButtonGLFW(b, a); };
static void OnMousePosition(GLFWwindow*, double x, double y) { TwEventMousePosGLFW((int)x, (int)y); };
static void OnMouseScroll(GLFWwindow*, double x, double y) { TwEventMouseWheelGLFW((int)y); };
static void OnKey(GLFWwindow*, int k, int s, int a, int m) { TwEventKeyGLFW(k, a); };
static void OnChar(GLFWwindow*, GLuint c) { TwEventCharGLFW(c, GLFW_PRESS); };
static void OnWindowResize(GLFWwindow*, int w, int h) { TwWindowSize(w, h); glViewport(0, 0, w, h); };