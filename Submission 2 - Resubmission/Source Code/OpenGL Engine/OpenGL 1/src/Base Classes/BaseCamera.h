#pragma once
#include <glm\glm.hpp>
#include "glm\gtx\transform.hpp"

using glm::mat3;
using glm::mat4;
using glm::vec3;
using glm::vec4;

class BaseCamera
{
public:
	BaseCamera();
	virtual ~BaseCamera();

	virtual void Update(float deltaTime) = 0;
	virtual void SetPerspective(float fov, float aspect, float near, float far);
	virtual void SetLookAt(vec3 to, vec3 up);
	virtual void SetPosition(vec3 position);
	virtual void SetTransform(mat4 newTrans);
	mat4 GetWorldTransform() { return worldTransform; }
	mat4 GetViewTransform() { return viewTransform; }
	mat4 GetProjectionTransform() { return projectionTransform; }
	mat4 GetProjectionViewTransform() { return projectionViewTransform; }
	vec3 GetPosition() { return vec3(worldTransform[3]); }

protected:
	mat4 viewTransform;
	mat4 projectionTransform;
	mat4 projectionViewTransform;
	void UpdateProjectionViewTransform();
	mat4 worldTransform;
};

