#pragma once
#include <gl_core_4_4.h>
#include <glm\vec4.hpp>
#include <glm\vec2.hpp>

class GeometryHelper
{
public:
	struct Vertex
	{
		glm::vec4 position;
		glm::vec4 colour;
		glm::vec2 texCoord;
	};

	GeometryHelper();
	~GeometryHelper();

	static void GenerateGrid(GLuint rows, GLuint columns, GLuint& VAO, GLuint& VBO, GLuint& IBO);
};