#include "Planet.h"

Planet::Planet()
{

}

Planet::Planet(mat4 transform, vec3 position, vec4 colour, float radius)
{
	// Set up the local variables to the variables you pass in
	localTransform = transform;
	localColour = colour;
	localTransform[3] = vec4(position,1);
	localRadius = radius;
	parent = nullptr;
	localRotation = 0;
}

Planet::~Planet()
{

}

void Planet::Update()
{
	// Safety checks
	if (parent == nullptr)
	{
		globalTransform = localTransform;
	}
	else
	{
		globalTransform = GetGlobalTransform() * localTransform;
	}
	rotationTransform *= glm::rotate(0.01f, vec3(0, 1, 0));
}

void Planet::Draw()
{
	// Draw out planet each time draw is called
	Gizmos::addSphere(vec3(globalTransform[3].x, globalTransform[3].y, globalTransform[3].z), localRadius, 10, 10, localColour, &rotationTransform);
}

mat4 Planet::GetGlobalTransform()
{
	if (parent == nullptr)
	{
		return localTransform * rotationTransform;
	}

	else
	{
		return parent->globalTransform * parent->rotationTransform;
	}
}

void Planet::AddParent(Planet* parentPlanet)
{
	this->parent = parentPlanet;
	parent->children.push_back(this);
}
