#pragma once

#include <iostream>
#include <vector>
#include "glm\glm.hpp"
#include "glm\gtx\transform.hpp"
#include "Gizmos.h"
#include "..\Base Classes\BaseCamera.h"

using glm::mat4;
using glm::vec4;
using glm::vec3;

class Planet
{
public:
	Planet(mat4 transform, vec3 position, vec4 colour, float radius);
	Planet();
	~Planet();
	
	void			Update();
	void			Draw();

	mat4			GetGlobalTransform();
	void			AddParent(Planet* parentPlanet);

	Planet*			parent;
	vec4			localColour;
	vec3			localPosition;
	float			localRotation;
	float			localRadius;

	mat4			globalTransform = mat4(1);
	mat4			rotationTransform = mat4(1);
	mat4			localTransform = mat4(1);

	std::vector<Planet*> planetList;
	std::vector<Planet*> children;
};