#include "SolarSystemApp.h"
#include "../Flying Camera Class/FlyCamera.h"
#include "Planet.h"
#include <cstddef>

SolarSystemApp::SolarSystemApp()
{

}

SolarSystemApp::~SolarSystemApp()
{

}

bool SolarSystemApp::Initialise()
{
	// Create new planets
	sun = new Planet(mat4(1), vec3(-3, 1, 0), vec4(1.f, 0.1f, 0.f, 1), 2);
	earth = new Planet(mat4(1), vec3(2, 0.25, 0), vec4(0.f, 0.5f, 0.7f, 1), 0.5);
	venus = new Planet(mat4(1), vec3(4.5, 0.5, 2), vec4(0.f, 0.9f, 0.4f, 1), 1);
	moon = new Planet(mat4(1), vec3(2, 0.25, 0.75), vec4(0.100f, 0.100f, .100f, 1), 0.25);

	captainPlanet = sun;
		
	earth->AddParent(sun);
	venus->AddParent(sun);
	moon->AddParent(earth);

	earth->localTransform[3].x += 3;
	venus->localTransform[3].x += 4;

	captainPlanet->planetList.push_back(moon);
	captainPlanet->planetList.push_back(sun);
	captainPlanet->planetList.push_back(earth);
	captainPlanet->planetList.push_back(venus);
	
	return true;
}

void SolarSystemApp::DeInitialise()
{
	delete sun;
	delete moon;
	delete earth;
	delete venus;
}

bool SolarSystemApp::Update(float deltaTime)
{
	// Impend rotation on the planet at the rotation (all planets rotate at the same speed)
	for (ptrdiff_t i = 0; i != (ptrdiff_t)captainPlanet->planetList.size(); i++)
	{
		captainPlanet->planetList.at(i)->localTransform *= glm::rotate(0.02f, vec3(0, 1, 0)); 
	}

	// Update each planet as it goes through the list
	for(Planet* iterator : captainPlanet->planetList)
	{
		iterator->Update();
	}

	return true;	
}

void SolarSystemApp::Draw(glm::mat4x4 projectionView)
{
	// Draw each planet in the least
	for (ptrdiff_t i = 0; i < (ptrdiff_t)captainPlanet->planetList.size(); i++)
	{
		captainPlanet->planetList[i]->Draw();
	}
}