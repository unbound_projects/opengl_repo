#pragma once
#include "../GameObject Class/BaseObject.h"

class FlyCamera;
class Planet;

class SolarSystemApp : public BaseObject
{
public:
	SolarSystemApp();
	~SolarSystemApp();

	bool				Initialise() override;
	void				DeInitialise() override;
	bool				Update(float deltaTime) override;
	void				Draw(glm::mat4x4 projectionView) override;

private:
	Planet*				captainPlanet;
	Planet*				sun;
	Planet*				moon;
	Planet*				earth;
	Planet*				venus;
};
