#pragma once
#include "FBX\FBXFile.h"
#include <glm\vec3.hpp>
#include <glm\ext.hpp>
#include <vector>

class SphereCollider
{
public:
	SphereCollider() : centre(0), radius(0) {}
	~SphereCollider() {}

	void Fit(const std::vector<glm::vec3>& points)
	{
		glm::vec3 min(1e37f), max(-1e37f);

		for (auto& p : points)
		{
			// Setting up the points to be the constant minimum/maximum after changes
			if (p.x < min.x) min.x = p.x;
			if (p.y < min.y) min.y = p.y;
			if (p.z < min.y) min.x = p.z;
			if (p.x > max.x) max.x = p.x;
			if (p.y > max.y) max.y = p.y;
			if (p.z > max.z) max.z = p.z;
		}

		centre = (min + max) * 0.5f;
		radius = glm::distance(min, centre);
	}	
	
	void Fit(const std::vector<FBXVertex>& vertices)
	{
		glm::vec3 min(1e37f), max(-1e37f);

		for (auto& vertex : vertices)
		{
			// Setting up the points to be the constant minimum/maximum after changes
			glm::vec4 p = vertex.position;

			if (p.x < min.x) min.x = p.x;
			if (p.y < min.y) min.y = p.y;
			if (p.z < min.z) min.z = p.z;
			if (p.x > max.x) max.x = p.x;
			if (p.y > max.y) max.y = p.y;
			if (p.z > max.z) max.z = p.z;
		}

		centre = (min + max) * 0.5f;
		radius = glm::distance(min, centre);
	}

	glm::vec3 centre;
	float radius;
};