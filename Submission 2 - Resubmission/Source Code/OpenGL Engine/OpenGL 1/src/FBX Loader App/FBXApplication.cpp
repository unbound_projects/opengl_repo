#include "FBXApplication.h"
#include "../Helper.h"
#include "../Base Classes/BaseApplication.h"
#include <stb-master\stb-master\stb_image.h>
#include <glm\gtc\type_ptr.hpp>

FBXApplication::FBXApplication(char* filePath, glm::vec3 position, int fbxID)
{
	m_filepath = filePath;
	m_position = position;
	m_globalTransform = glm::translate(m_position);
	m_fbxID = fbxID;
	storedScale = BaseApplication::gui_FBXScaleRate;
}

FBXApplication::~FBXApplication()
{

}

bool FBXApplication::Initialise()
{
	Create(m_filepath.c_str());

	if(m_fbxID == 3)
		collider->radius *= BaseApplication::gui_SkyboxScaleRate;

	return true;
}

void FBXApplication::DeInitialise()
{
	cleanupOpenGLBuffers(m_fbx);
	delete m_fbx;
	delete collider;
}

bool FBXApplication::Update(float deltaTime)
{
	if (m_fbxID == 1 || m_fbxID == 2)
	{
		if (storedScale != BaseApplication::gui_FBXScaleRate)
		{
			BaseApplication::fbxScaleHasChanged = true;
			if (BaseApplication::fbxScaleHasChanged == true)
			{
				collider->radius *= BaseApplication::gui_FBXScaleRate;
			}
			BaseApplication::fbxScaleHasChanged = false;
			storedScale = BaseApplication::gui_FBXScaleRate;
		}

	}
	if (m_fbxID == 1)
	{	
		m_position = BaseApplication::gui_FBX1Position;
		m_globalTransform = glm::translate(m_position);
	}

	if (m_fbxID == 2)
	{
		m_position = BaseApplication::gui_FBX2Position;
		m_globalTransform = glm::translate(m_position);
	}

	return true;
}

void FBXApplication::Draw(glm::mat4x4 projectionView)
{
	GLuint programID = m_program.GetID();

	glUseProgram(programID);

	//bind the camera
	int iLocation = glGetUniformLocation(programID, "ProjectionView");
	glUniformMatrix4fv(iLocation, 1, GL_FALSE, glm::value_ptr(projectionView));
	
	// cleanup code ------------ bind the rest of the uniforms
	BindUniforms(programID, m_camera->GetPosition());

	//bind our vertex array object and draw the mesh
	
	for (unsigned int j = 0; j < m_fbx->getMeshCount(); j++)
	{
		FBXMeshNode* mesh = m_fbx->getMeshByIndex(j);

		unsigned int* glData = (unsigned int*)mesh->m_userData;

		glBindVertexArray(glData[0]);
		glDrawElements(GL_TRIANGLES, (unsigned int)mesh->m_indices.size(), GL_UNSIGNED_INT, 0);
	}	
}

#pragma region CUSTOM FUNCTIONS
void FBXApplication::createOpenGLBuffers(FBXFile* fbx)
{
	// create the GL VAO/VBO/IBO data for each mesh
	for (unsigned int i = 0; i < fbx->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = fbx->getMeshByIndex(i);

		unsigned int* glData = new unsigned int[3];

		glGenVertexArrays(1, &glData[0]);
		glGenBuffers(1, &glData[1]);
		glGenBuffers(1, &glData[2]);

		glBindVertexArray(glData[0]);
		glBindBuffer(GL_ARRAY_BUFFER, glData[1]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glData[2]);

		glBufferData(GL_ARRAY_BUFFER, mesh->m_vertices.size() * sizeof(FBXVertex),
									  mesh->m_vertices.data(), GL_STATIC_DRAW);

		glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->m_indices.size() * sizeof(unsigned int),
											  mesh->m_indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), 0);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::NormalOffset);

		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::TexCoord1Offset);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		mesh->m_userData = glData;
	}
}

void FBXApplication::Create(const char* filePath)
{
	// Everytime create is called, create a new FBX file & load in from filepath
	m_fbx = new FBXFile();
	m_fbx->load(filePath, FBXFile::UNITS_METER, false);
	
	createOpenGLBuffers(m_fbx);

	GenerateShaders();
	GenerateTextures();

	collider = new SphereCollider();	

	// For each mesh in the fbx file, fit a collider for frustum culling & collision detection
	for (unsigned int i = 0; i < m_fbx->getMeshCount(); i++)
	{
		FBXMeshNode* mesh = m_fbx->getMeshByIndex(i);

		collider->Fit(mesh->m_vertices);
	}

	SetCamera(m_camera);
}

void FBXApplication::cleanupOpenGLBuffers(FBXFile* fbx)
{
	// iterate through the fbx mesh count and delete all the buffers, mesh's and data
	for (unsigned int i = 0; i < fbx->getMeshCount(); i++)
	{
		FBXMeshNode* mesh = fbx->getMeshByIndex(i);

		unsigned int* glData = (unsigned int*)mesh->m_userData;

		glDeleteVertexArrays(1, &glData[0]);
		glDeleteBuffers(1, &glData[1]);
		glDeleteBuffers(1, &glData[2]);

		delete[] glData;
	}
}

void FBXApplication::GenerateShaders()
{
	if (m_fbxID == 1 || m_fbxID == 2)
	{
		const char* vsSource = R"(
				#version 410
				layout(location=0) in vec4 Position;
				layout(location=1) in vec4 Normal;
				layout(location=2) in vec2 TexCoord;
				out vec4 vNormal;
				out vec4 vPosition;
				out vec2 vTexCoord;
	
				uniform mat4 ProjectionView;
				uniform mat4 GlobalTransform;
				
				uniform float scaleRate;				

				void main()
				{
					mat4 scaledGT = GlobalTransform;

					scaledGT[0][0] *= scaleRate;
					scaledGT[1][1] *= scaleRate;
					scaledGT[2][2] *= scaleRate;

					vNormal = Normal;
					vTexCoord = TexCoord;
					vPosition = Position;
					gl_Position = ProjectionView * scaledGT * Position;
				}
			)";
	
		const char* fsSource = R"(
				#version 410
				in vec4 vNormal;
				in vec4 vPosition;
				in vec2 vTexCoord;
				out vec4 FragColour;
				uniform vec3 LightDir;
				uniform vec3 CameraPos;
	
				uniform sampler2D fbxTex;
	
				uniform vec3 kA = vec3(1, 0, 0); //Red ambient material colour
				uniform vec3 kD = vec3(1, 0, 0); //Red diffuse material colour
				uniform vec3 kS = vec3(1, 0, 0); //Red specular material colour
	
				uniform vec3 iA = vec3(0.25f, 0.25f, 0.25f);
				uniform vec3 iD = vec3(1, 1, 1);
				uniform vec3 iS = vec3(1, 1, 1);
	
				uniform float SpecPow = 32.0f;
	
				void main()
				{				
					FragColour = texture(fbxTex, vTexCoord);
	
					vec3 Ambient = kA * iA; // ambient light
	
					float NdL = max(0.0f, dot(vNormal.xyz, -LightDir));
					vec3 Diffuse = kD * iD * NdL;
	
					vec3 R = reflect(-LightDir, vNormal.xyz);
					vec3 E = normalize(CameraPos - vPosition.xyz);
	
					float specTerm = pow(max(0.0f, dot(R, E)), SpecPow);
					vec3 Specular = kS * iS * specTerm;
	
					FragColour *= vec4(Ambient + Diffuse + Specular, 1);
				}
			)";
	
		m_program.Create(vsSource, fsSource);
	}

	else if (m_fbxID == 3)
	{ 
		const char* vsSource = R"(
				#version 410
				layout(location=0) in vec4 Position;
				layout(location=1) in vec4 Normal;
				layout(location=2) in vec2 TexCoord;
				out vec4 vNormal;
				out vec4 vPosition;
				out vec2 vTexCoord;
	
				uniform mat4 ProjectionView;
				uniform mat4 GlobalTransform;
			
				uniform float scaleRate;
	
				void main()
				{
					mat4 scaledGlobalTransform = GlobalTransform;

					scaledGlobalTransform[0][0] *= scaleRate;
					scaledGlobalTransform[1][1] *= scaleRate;
					scaledGlobalTransform[2][2] *= scaleRate;
							
					vNormal = Normal;
					vTexCoord = TexCoord;
					vPosition = Position;
					gl_Position = ProjectionView * scaledGlobalTransform * Position;
				}
			)";

		const char* fsSource = R"(
				#version 410
				in vec4 vNormal;
				in vec4 vPosition;
				in vec2 vTexCoord;
				out vec4 FragColour;
	
				uniform sampler2D fbxTex;
	
				void main()
				{				
					FragColour = texture(fbxTex, vTexCoord);
				}
			)";

		m_program.Create(vsSource, fsSource);
	}
}

void FBXApplication::GenerateTextures()
{
	if (m_fbxID == 1)
	{
		// Set up the texture variables, load in the texture from the filepath
		int imageHeight = 0, imageWidth = 0, imageFormat = 0;
		unsigned char* data = stbi_load("../data/models/Stanford/soulspear/soulspear_diffuse.tga", &imageHeight, &imageWidth, &imageFormat, STBI_default);
		
		// Create and bind the openGL texture to the m_texture variable
		glActiveTexture(GL_TEXTURE0);
		glGenTextures(1, &m_texture);
		glBindTexture(GL_TEXTURE_2D, m_texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageHeight, imageWidth, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// Data is stored, can free.
		stbi_image_free(data);

		unsigned int programID = m_program.GetID();

		glUseProgram(programID);

		int iLocation = glGetUniformLocation(programID, "fbxTex");
		assert(iLocation != -1 && "fbxTex uniform could not be found.");
		glUniform1i(iLocation, 0);
	}

	else if (m_fbxID == 2)
	{
		// Set up the texture variables, load in the texture from the filepath
		int imageHeight = 0, imageWidth = 0, imageFormat = 0;
		unsigned char* data = stbi_load("../data/textures/grass_d.jpg", &imageHeight, &imageWidth, &imageFormat, STBI_default);

		// Create and bind the openGL texture to the m_texture variable
		glActiveTexture(GL_TEXTURE0);
		glGenTextures(1, &m_texture);
		glBindTexture(GL_TEXTURE_2D, m_texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageHeight, imageWidth, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		// Data is stored, can free.
		stbi_image_free(data);

		unsigned int programID = m_program.GetID();

		glUseProgram(programID);

		int iLocation = glGetUniformLocation(programID, "fbxTex");
		assert(iLocation != -1 && "fbxTex uniform could not be found.");
		glUniform1i(iLocation, 0);
	}

	else if (m_fbxID == 3)
	{
		// Set up the texture variables, load in the texture from the filepath
		int imageHeight = 0, imageWidth = 0, imageFormat = 0;
		unsigned char* data = stbi_load("../data/textures/sky.jpg", &imageHeight, &imageWidth, &imageFormat, STBI_default);

		// Create and bind the openGL texture to the m_texture variable
		glActiveTexture(GL_TEXTURE0);
		glGenTextures(1, &m_texture);
		glBindTexture(GL_TEXTURE_2D, m_texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageHeight, imageWidth, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		// Data is stored, can free.
		stbi_image_free(data);

		unsigned int programID = m_program.GetID();

		glUseProgram(programID);

		int iLocation = glGetUniformLocation(programID, "fbxTex");
		assert(iLocation != -1 && "fbxTex uniform could not be found.");
		glUniform1i(iLocation, 0);
	}	
}

void FBXApplication::BindUniforms(GLuint programID, glm::vec3 flycamPos)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_texture);

	if (m_fbxID == 1 || m_fbxID == 2)
	{
		//set the uniform 'lightDir' and its values
		int iLocation = glGetUniformLocation(programID, "LightDir");
		assert(iLocation != -1 && "LightDir uniform could not be found.");
		glUniform3fv(iLocation, 1, glm::value_ptr(BaseApplication::gui_FBXLightDirection));

		//set the uniform 'CameraPos' and it's values
		iLocation = glGetUniformLocation(programID, "CameraPos");
		assert(iLocation != -1 && "CameraPos uniform could not be found.");
		glUniform3fv(iLocation, 1, glm::value_ptr(flycamPos));

		//set the uniform 'GlobalTransform' and its values
		iLocation = glGetUniformLocation(programID, "GlobalTransform");
		assert(iLocation != -1 && "GlobalTransform uniform could not be found.");
		glUniformMatrix4fv(iLocation, 1, GL_FALSE, glm::value_ptr(m_globalTransform));

		// Set up the lighting uniforms
		iLocation = glGetUniformLocation(programID, "kA");
		assert(iLocation != -1 && "Ambient Lighting uniform could not be found.");
		glUniform3fv(iLocation, 1, glm::value_ptr(BaseApplication::gui_FBXAmbientColour));

		iLocation = glGetUniformLocation(programID, "kD");
		assert(iLocation != -1 && "Diffuse Lighting uniform could not be found.");
		glUniform3fv(iLocation, 1, glm::value_ptr(BaseApplication::gui_FBXDiffuseColour));

		iLocation = glGetUniformLocation(programID, "kS");
		assert(iLocation != -1 && "Specular Colour Lighting uniform could not be found.");
		glUniform3fv(iLocation, 1, glm::value_ptr(BaseApplication::gui_FBXSpecularColour));

		iLocation = glGetUniformLocation(programID, "SpecPow");
		assert(iLocation != -1 && "Specular Power Lighting uniform could not be found.");
		glUniform1f(iLocation, BaseApplication::gui_FBXSpecularPower);

		iLocation = glGetUniformLocation(programID, "scaleRate");
		assert(iLocation != -1 && "Scale Rate Uniform could not be found.");
		glUniform1f(iLocation, BaseApplication::gui_FBXScaleRate);
	}

	if (m_fbxID == 3)
	{
		int iLocation = glGetUniformLocation(programID, "scaleRate");
		if (iLocation != -1)
		{
			glUniform1f(iLocation, BaseApplication::gui_SkyboxScaleRate);
		}

		//set the uniform 'GlobalTransform' and its values
		iLocation = glGetUniformLocation(programID, "GlobalTransform");
		assert(iLocation != -1 && "GlobalTransform uniform could not be found.");
		glUniformMatrix4fv(iLocation, 1, GL_FALSE, glm::value_ptr(m_globalTransform));
	}
}
#pragma endregion
