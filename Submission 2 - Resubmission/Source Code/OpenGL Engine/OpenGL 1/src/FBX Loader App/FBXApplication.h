#pragma once
#include "../GameObject Class/BaseObject.h"
#include "../Program Helper Class/ProgramHelper.h"
#include "../Collision and Culling Class/SphereCollider.h"
#include "FBX\FBXFile.h"
#include <vector>

class FBXApplication : public BaseObject
{
public:	
	FBXApplication(char* filePath, glm::vec3 position, int fbxID);
	~FBXApplication();

	bool				Initialise();
	void				DeInitialise();
	bool				Update(float deltaTime);
	void				Draw(glm::mat4x4 projectionView);

	void				Create(const char* filePath);

	// Custom Functions
	void				createOpenGLBuffers(FBXFile* fbx);
	void				cleanupOpenGLBuffers(FBXFile* fbx);
	void				GenerateShaders();
	void				GenerateTextures();
	void				BindUniforms(GLuint programID, glm::vec3 flycamPos);

	FBXFile*			m_fbx;
	ProgramHelper		m_program;	
	std::string			m_filepath;
	GLuint				m_texture;
	int					m_fbxID;
	glm::vec3			m_position;
	float				storedScale;
};