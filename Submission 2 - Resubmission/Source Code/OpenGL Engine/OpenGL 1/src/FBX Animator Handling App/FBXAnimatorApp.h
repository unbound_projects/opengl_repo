#pragma once
#include "../GameObject Class/BaseObject.h"
#include "../Program Helper Class/ProgramHelper.h"
#include "FBX\FBXFile.h"
#include <glm\mat4x4.hpp>

class FBXAnimatorApp : public BaseObject
{
public:
	FBXAnimatorApp();
	~FBXAnimatorApp();

	// Inherited functions
	bool				Initialise();
	void				DeInitialise();
	bool				Update(float deltaTime);
	void				Draw(glm::mat4x4 projectionView);

	// Custom functions
	void				createOpenGLBuffers(FBXFile* fbx);
	void				cleanupOpenGLBuffers(FBXFile* fbx);
	void				GenerateShaders();	

private:
	FBXFile*			m_fbx_file;
	GLfloat				m_timer;
	glm::mat4			m_globalTransform = glm::mat4(1);
	GLuint				m_diffuseTex;
	ProgramHelper		m_program;

	// Buffers
	GLuint				m_VAO;
	GLuint				m_VBO;
	GLuint				m_IBO;
};