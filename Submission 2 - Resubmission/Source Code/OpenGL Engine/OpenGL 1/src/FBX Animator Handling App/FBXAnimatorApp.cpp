#include "FBXAnimatorApp.h"
#include "../Helper.h"
#include <stb-master\stb-master\stb_image.h>
#include <glm\gtc\type_ptr.hpp>

FBXAnimatorApp::FBXAnimatorApp()
{
	m_timer = 0;
}

FBXAnimatorApp::~FBXAnimatorApp()
{

}

bool FBXAnimatorApp::Initialise()
{
	// set up the texture image stuff
	int imageWidth = 0, imageHeight = 0, imageFormat = 0;

	// create the new FBX file, load in from file path, initialise openGL textures and create openGL buffers
	m_fbx_file = new FBXFile();
	m_fbx_file->load("../data/models/Stanford/Pyro/pyro.fbx");
	m_fbx_file->initialiseOpenGLTextures();
	createOpenGLBuffers(m_fbx_file);

	GenerateShaders();
	
	return true;
}

void FBXAnimatorApp::DeInitialise()
{
	cleanupOpenGLBuffers(m_fbx_file);
	delete m_fbx_file;
}

bool FBXAnimatorApp::Update(float deltaTime)
{
	m_timer += deltaTime;

	// Find the skeleton and animation inside the FBX file
	FBXSkeleton* skeleton = m_fbx_file->getSkeletonByIndex(0);
	FBXAnimation* animation = m_fbx_file->getAnimationByIndex(0);

	// Evaluate the animation over the timer
	skeleton->evaluate(animation, m_timer);
	
	// For every bone in the skeleton, update their global transform
	for (GLuint bone_index = 0; bone_index < skeleton->m_boneCount; bone_index++)
	{
		skeleton->m_nodes[bone_index]->updateGlobalTransform();
	}

	return true;
}

void FBXAnimatorApp::Draw(glm::mat4x4 projectionView)
{
	GLuint programID = m_program.GetID();

	glUseProgram(programID);

	// bind the camera, crash if the location can't be found
	int iLocation = glGetUniformLocation(programID, "ProjectionView");
	assert(iLocation != -1 && "If iLocation is negative one here, the projection view matrix could not be found.");
	glUniformMatrix4fv(iLocation, 1, GL_FALSE, glm::value_ptr(projectionView));

	// bind the diffuse, crash if can't find diffuse
	iLocation = glGetUniformLocation(programID, "diffuse");
	assert(iLocation != -1 && "If iLocation is -1 here, the diffuse is not set up properly.");
	glUniform1i(iLocation, 0);

	// Find the skeleton, update the bones
	FBXSkeleton* skeleton = m_fbx_file->getSkeletonByIndex(0); 
	skeleton->updateBones();

	// bind the uniform for the bones
	int bones_location = glGetUniformLocation(programID, "bones");
	glUniformMatrix4fv(bones_location, skeleton->m_boneCount, GL_FALSE, (float*)skeleton->m_bones);

	for (unsigned int i = 0; i < m_fbx_file->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = m_fbx_file->getMeshByIndex(i);

		FBXTexture* pTex = mesh->m_material->textures[FBXMaterial::DiffuseTexture];

		if (pTex) {
			// set the diffuse slot
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, pTex->handle);
		}

		// create the UI Buffers and save them to user data
		unsigned int* uiBuffers = (unsigned int*)mesh->m_userData;
		glBindVertexArray(uiBuffers[0]);
		glDrawElements(GL_TRIANGLES, (unsigned int)mesh->m_indices.size(), GL_UNSIGNED_INT, 0);
	}
}

void FBXAnimatorApp::createOpenGLBuffers(FBXFile * fbx)
{
	for (unsigned int i = 0; i < fbx->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = fbx->getMeshByIndex(i);
		unsigned int* uiBuffers = new unsigned int[3];

		// Generate Vertex Objects & Index Buffer
		glGenVertexArrays(1, &uiBuffers[0]);
		glGenBuffers(1, &uiBuffers[1]);
		glGenBuffers(1, &uiBuffers[2]);

		// Bind Vertex Objects to variables
		glBindVertexArray(uiBuffers[0]);
		glBindBuffer(GL_ARRAY_BUFFER, uiBuffers[1]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(FBXVertex) * mesh->m_vertices.size(), mesh->m_vertices.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, uiBuffers[2]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * mesh->m_indices.size(), mesh->m_indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); // Position
		glEnableVertexAttribArray(1); // Normal
		glEnableVertexAttribArray(2); // Tangent
		glEnableVertexAttribArray(3); // Tex Coord
		glEnableVertexAttribArray(4); // Weights
		glEnableVertexAttribArray(5); // Indices

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), 0); //(void*)FBXVertex::PositionOffset);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), (void*)FBXVertex::NormalOffset);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), (void*)FBXVertex::TangentOffset);
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::TexCoord1Offset);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::WeightsOffset);
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::IndicesOffset);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		mesh->m_userData = uiBuffers;
	}
}

void FBXAnimatorApp::cleanupOpenGLBuffers(FBXFile * fbx)
{ 
	for (unsigned int i = 0; i < fbx->getMeshCount(); i++)
	{
		FBXMeshNode* mesh = fbx->getMeshByIndex(i);

		unsigned int* glData = (unsigned int*)mesh->m_userData;

		glDeleteVertexArrays(1, &glData[0]);
		glDeleteBuffers(1, &glData[1]);
		glDeleteBuffers(1, &glData[2]);

		delete[] glData;
	}
}

void FBXAnimatorApp::GenerateShaders()
{
	// *********** CREATE THE VERTEX SHADER ****************
	const char* vsShader = R"(
		#version 410
		layout(location = 0) in vec4 Position;
		layout(location = 1) in vec4 Normal;
		layout(location = 2) in vec4 Tangent;
		layout(location = 3) in vec2 TextureCoord;
		layout(location = 4) in vec4 Weights;
		layout(location = 5) in vec4 Indices;
		//
		out vec2 vTexCoord;
		out vec3 vPosition;
		out vec3 vNormal;
		out vec3 vTangent;
		out vec3 vBiTangent;
		//
		uniform mat4 ProjectionView;
		uniform mat4 Global;
		const int MAX_BONES = 128;
		uniform mat4 bones[MAX_BONES];
		//
		void main()
		{
			vPosition = Position.xyz;
			vNormal = Normal.xyz;
			vTangent = Tangent.xyz;			
			vBiTangent = cross(Normal.xyz, Tangent.xyz);
			vTexCoord = TextureCoord;

			ivec4 index = ivec4(Indices);

			vec4 P = bones[index.x] * Position * Weights.x;
			P += bones[index.y] * Position * Weights.y;
			P += bones[index.z] * Position * Weights.z;
			P += bones[index.w] * Position * Weights.w;
			gl_Position = ProjectionView * P;
		}
	)";

	// **************** CREATE THE FRAGMENT SHADER******************
	const char* fsShader =
		R"(
			#version 410
			in vec4 vNormal;
			in vec4 vPosition;
			in vec2 vTexCoord;
			out vec4 FragColour;
			uniform vec3 LightDir;
			uniform vec3 LightColour;
			uniform vec3 CameraPos;
			uniform sampler2D diffuse;
			uniform float SpecPow;
			void main()
			{
				float d = max(0, dot(normalize(vNormal.xyz), LightDir));
				vec3 E = normalize(CameraPos - vPosition.xyz);
				vec3 R = reflect(-LightDir, vNormal.xyz);
				float s = max(0, dot(E, R));
				s = pow(s, SpecPow);
				FragColour = texture(diffuse, vTexCoord); 
			}
		)";
	
	m_program.Create(vsShader, fsShader);
}
