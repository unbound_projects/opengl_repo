#include "GenerationApp.h"
#include "../Helper.h"
#include "../GeometryHelper.h"
#include "../Base Classes/BaseApplication.h"
#include <glm\vec4.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\noise.hpp>
#include <stb-master\stb-master\stb_image.h>
#include <glfw3.h>
#include <time.h>

bool GenerationApp::Initialise()
{
	srand((unsigned int)time(0));

	GenerateHeightMap(1000, 1000);
	GeometryHelper::GenerateGrid(1000, 1000, m_WaterVAO, m_WaterVBO, m_WaterIBO);

	GenerateShaders();
	GenerateWaterShader();
	GenerateTextures();
	GenerateWaterTexture();

	return true;
}

void GenerationApp::DeInitialise()
{
	delete perlin_data;
}

bool GenerationApp::Update(float deltaTime)
{
	double currentFrame = glfwGetTime();
	DT = currentFrame - pastFrame;
	pastFrame = currentFrame;

	return true;
}

void GenerationApp::Draw(glm::mat4x4 projectionView)
{
	GLuint programID = m_program.GetID();
	GLuint waterProgramID = m_waterProgram.GetID();
	
	glUseProgram(programID);

	//bind the camera
	int ProceduralILocation = glGetUniformLocation(programID, "ProjectionView");
	assert(ProceduralILocation != -1 && "If -1, we cannot find the ProjectionView matrix.");
	glUniformMatrix4fv(ProceduralILocation, 1, GL_FALSE, glm::value_ptr(projectionView));

	BindTexturesAndUniforms(programID, m_camera->GetPosition());

	// draw the texture
	glBindVertexArray(m_ProceduralVAO);
	glDrawElements(GL_TRIANGLES, m_uiGridIndexCount, GL_UNSIGNED_INT, nullptr); 	

	glUseProgram(waterProgramID);

	//bind the camera
	int waterILocation = glGetUniformLocation(waterProgramID, "ProjectionView");
	assert(waterILocation != -1 && "If -1, we cannot find the ProjectionView matrix.");
	glUniformMatrix4fv(waterILocation, 1, GL_FALSE, glm::value_ptr(projectionView));
	
	BindWaterTexturesAndUniforms(waterProgramID);

	glBindVertexArray(m_WaterVAO);
	glDrawElements(GL_TRIANGLES, m_uiGridIndexCount, GL_UNSIGNED_INT, nullptr);

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
}

void GenerationApp::GenerateHeightMap(GLuint rows, GLuint columns)
{
	GeneratePerlinNoise();

	// create array of vertices to pass to the GPU
	Vertex* verts = new Vertex[rows * columns];

	for (int iter_rows = 0; iter_rows < rows; iter_rows++)
	{
		for (int iter_columns = 0; iter_columns < columns; iter_columns++)
		{
			int vertsIndex = iter_rows * columns + iter_columns;

			verts[vertsIndex].position = glm::vec4((float)iter_columns, 0, (float)iter_rows, 1);

			verts[vertsIndex].texCoord = glm::vec2((float)iter_columns / (float)columns, (float)iter_rows / (float)rows);

			verts[vertsIndex].texCoord2 = glm::vec2(verts[vertsIndex].position.x / 75, verts[vertsIndex].position.z / 75);

			// create some arbitrary colour based off something that might not be related to tiling a texture
			glm::vec3 colour = glm::vec3(sinf((iter_columns / (float)(columns - 1)) * (iter_rows / (float)(rows - 1))));

			verts[iter_rows * columns + iter_columns].colour = glm::vec4(colour, 1);
		}
	}

	m_uiGridIndexCount = (rows - 1) * (columns - 1) * 6;
	GLuint* indices = new GLuint[m_uiGridIndexCount];

	GLuint index = 0;
	for (unsigned int iter_rows = 0; iter_rows < (rows - 1); ++iter_rows)
	{
		for (unsigned int iter_columns = 0; iter_columns < (columns - 1); ++iter_columns)
		{
			//triangle one
			indices[index++] = iter_rows * columns + iter_columns;
			indices[index++] = (iter_rows + 1) * columns + iter_columns;
			indices[index++] = (iter_rows + 1) * columns + (iter_columns + 1);

			//triangle two
			indices[index++] = iter_rows * columns + iter_columns;
			indices[index++] = (iter_rows + 1) * columns + (iter_columns + 1);
			indices[index++] = iter_rows * columns + (iter_columns + 1);
		}
	}

	glGenVertexArrays(1, &m_ProceduralVAO);
	glBindVertexArray(m_ProceduralVAO);

	glGenBuffers(1, &m_ProceduralVBO);
	glGenBuffers(1, &m_ProceduralIBO);

	glBindBuffer(GL_ARRAY_BUFFER, m_ProceduralVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ProceduralIBO);

	glBufferData(GL_ARRAY_BUFFER, (rows * columns) * sizeof(Vertex), verts, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (rows - 1) * (columns - 1) * 6 * sizeof(unsigned int), indices, GL_STATIC_DRAW);
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, colour));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoord));
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoord2));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	delete[] indices;
	delete[] verts;
}

void GenerationApp::GeneratePerlinNoise()
{
	m_seed = 1 + ((rand() % 200) / 100.0f);

	//Generate perlin noise
	int dims = 64;
	perlin_data = new float[64 * 64];
	float scale = (1.0f / dims) * 3;
	int octaves = 6;

	// Iterate through the perlin noise
	for (int x = 0; x < 64; x++)
	{
		for (int y = 0; y < 64; y++)
		{
			// Add amplitude and persistence, set the data to be null
			float amplitude = 22.0f;
			float persistence = 0.4f;
			perlin_data[y * dims + x] = 0;

			// Iterate through the octaves of the perlin noise
			for (int o = 0; o < octaves; o++)
			{
				// Add data to the perlin data based on frequency and the perlin seeds
				float freq = powf(2, (float)o);
				float perlin_sample = glm::perlin(glm::vec2((float)x * m_seed, (float)y * m_seed) * scale * freq) * 1.5f + .65f;
				perlin_data[y * dims + x] += perlin_sample * amplitude;
				amplitude *= persistence;
			}
		}
	}
}

void GenerationApp::RegenerateNoise()
{
	// Clear the buffers for the procedural generation buffers
	glDeleteBuffers(1, &m_ProceduralIBO);
	glDeleteBuffers(1, &m_ProceduralVBO);
	glDeleteVertexArrays(1, &m_ProceduralVAO);

	// Create a new height map of the same size
	GenerateHeightMap(1000, 1000);

	// Re-activate the perlin noise texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_perlin_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 64, 64, 0, GL_RED, GL_FLOAT, perlin_data);
}

void GenerationApp::GenerateTextures()
{
	int imageWidth = 0, imageHeight = 0, imageFormat = 0;

	// THIS IS FOR PERLIN NOISE TEXTURING
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &m_perlin_texture);
	glBindTexture(GL_TEXTURE_2D, m_perlin_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 64, 64, 0, GL_RED, GL_FLOAT, perlin_data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// this is for texturing over the perlin noise [ground texture]
	unsigned char* data = stbi_load("../data/textures/grass_d.jpg", &imageWidth, &imageHeight, &imageFormat, STBI_default);

	glActiveTexture(GL_TEXTURE1);
	glGenTextures(1, &m_ground_texture);
	glBindTexture(GL_TEXTURE_2D, m_ground_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	stbi_image_free(data);

	// this is for texturing over the perlin noise [water texture]
	data = stbi_load("..\\data\\textures\\water_d.jpg", &imageWidth, &imageHeight, &imageFormat, STBI_default);

	glActiveTexture(GL_TEXTURE2);
	glGenTextures(1, &m_water_texture);
	glBindTexture(GL_TEXTURE_2D, m_water_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	stbi_image_free(data);

	// this is for texturing over the perlin noise [snow texture]
	data = stbi_load("..\\data\\textures\\snow_d.jpg", &imageWidth, &imageHeight, &imageFormat, STBI_default);

	glActiveTexture(GL_TEXTURE3);
	glGenTextures(1, &m_snow_texture);
	glBindTexture(GL_TEXTURE_2D, m_snow_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	stbi_image_free(data);

	// this is for texturing over the perlin noise [snow texture]
	data = stbi_load("..\\data\\textures\\stone_d.jpg", &imageWidth, &imageHeight, &imageFormat, STBI_default);

	glActiveTexture(GL_TEXTURE4);
	glGenTextures(1, &m_stone_texture);
	glBindTexture(GL_TEXTURE_2D, m_stone_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	stbi_image_free(data);

	// this is for texturing in some sand
	data = stbi_load("..\\data\\textures\\sand_d.jpg", &imageWidth, &imageHeight, &imageFormat, STBI_default);

	glActiveTexture(GL_TEXTURE5);
	glGenTextures(1, &m_sand_texture);
	glBindTexture(GL_TEXTURE_2D, m_sand_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	stbi_image_free(data);
}

void GenerationApp::GenerateShaders()
{
	const char* vertexShaderSource = R"(
		#version 410
		layout(location=0) in vec4 Position;
		layout(location=1) in vec4 Colour;
		layout(location=2) in vec2 TexCoord1;
		layout(location=3) in vec2 TexCoord2;
		layout(location=4) in vec4 Normal;
		
		uniform mat4 ProjectionView;
		uniform sampler2D perlin_texture;

		out vec2 vTexCoord1;
		out vec2 vTexCoord2;
		out vec4 vColour;
		out float MaxHeight;
		out vec4 vNormal;
		out vec4 vPosition;
		
		void main()
		{
			vec4 pos = Position;
			pos.y += texture(perlin_texture, TexCoord1).r * 5;
			vTexCoord1 = TexCoord1;
			vTexCoord2 = TexCoord2;
			vColour = Colour;
			vNormal = Normal;
			gl_Position = ProjectionView * pos;
			vPosition = pos;
			MaxHeight = pos.y;
		}
	)";

	const char* fragmentShaderSource = R"(
		#version 410
		in vec2 vTexCoord1;
		in vec2 vTexCoord2;
		in vec4 vColour;
		in float MaxHeight;
		in vec4 vNormal;
		in vec4 vPosition;

		out vec4 out_colour;
		out vec4 out_lighting;

				// TEXTURING
		uniform sampler2D perlin_texture;
		uniform sampler2D textureGround;
		uniform sampler2D textureSnow;
		uniform sampler2D textureStone;
		uniform sampler2D textureSand;

		const float maxGrassHeight = 90;
		const float minGrassHeight = 55;

		const float maxStoneHeight = 130;
		const float minStoneHeight = 115;

		const float maxSandHeight = 50;
		const float minSandHeight = 45;

		const float minWaterHeight = 0;
		const float maxWaterHeight = 35;

		const float maxSnowHeight = 200;
		const float minSnowHeight = 160;

				// LIGHTING
		uniform vec3 LightDirection = vec3(0, -1, 0);
		uniform vec3 kA = vec3(1, 0, 0);
		uniform vec3 kD = vec3(1, 1, 1);
		uniform vec3 iA = vec3(1, 1, 1);
		uniform vec3 iD = vec3(1, 1, 1);

		void main()
		{
			// ********** BLENDING & TEXTURING **************
			float blendRate = (MaxHeight - maxGrassHeight) / (minStoneHeight - maxGrassHeight);
			blendRate = clamp(blendRate, 0, 1);

			vec3 groundColour = texture(textureGround, vTexCoord2).rgb;
			vec3 stoneColour = texture(textureStone, vTexCoord2).rgb;
			vec3 snowColour = texture(textureSnow, vTexCoord2).rgb;
			vec3 sandColour = texture(textureSand, vTexCoord2).rgb;

			out_colour.rgb = stoneColour * blendRate + groundColour * (1 - blendRate);
			out_colour.a = 1;

			blendRate = (MaxHeight - maxSandHeight) / (minGrassHeight - maxSandHeight) / 1.5f;
			blendRate = clamp(blendRate, 0, 1);

			out_colour.rgb = out_colour.rgb * blendRate + sandColour * (1 - blendRate);

			blendRate = (MaxHeight - maxWaterHeight) / (minSandHeight - maxWaterHeight) / 1.5f;
			blendRate = clamp(blendRate, 0, 1);

			out_colour.rgb = out_colour.rgb * blendRate + sandColour * (1 - blendRate);

			blendRate = (MaxHeight - maxStoneHeight) / (minSnowHeight - maxStoneHeight) / 3.0f;
			blendRate = clamp(blendRate, 0, 1);

			out_colour.rgb = snowColour * blendRate + out_colour.rgb * (1 - blendRate);

			// ********* LIGHTING ***************
			vec3 Ambient = kA * iA; // ambient light

			float NdL = max(0.0f, dot(vNormal.xyz, -LightDirection));
			vec3 Diffuse = kD * iD * NdL;

			out_colour *= vec4(Ambient + Diffuse + 0, 1);
		}
	)";

	m_program.Create(vertexShaderSource, fragmentShaderSource);
}

void GenerationApp::GenerateWaterShader()
{
	const char* waterVertexShader = R"(
		#version 410
		layout(location = 0) in vec4 Position;
		layout(location = 1) in vec4 Colour;
		layout(location = 2) in vec2 TexCoord2;
//
		out vec2 vTexCoord2;
//
		uniform mat4 ProjectionView;
		uniform float waterHeight;
		uniform float time;
		uniform float heightScale;
//
		void main()
		{
			vec4 pos = Position;
			pos.y = waterHeight;
			pos.y += sin(time + Position.x * 0.025) * heightScale;
			vTexCoord2 = TexCoord2;
			gl_Position = ProjectionView * pos;
		}
	)";

	const char* waterFragmentShader = R"(
		#version 410
		in vec4 vPosition;
		in vec2 vTexCoord2;
//
		out vec4 out_colour;
//
		uniform sampler2D textureWater;
		uniform float opaqueLevel;
//
		void main()
		{			
			out_colour = texture(textureWater, vTexCoord2);
			out_colour.a = opaqueLevel;
		}
	)";

	m_waterProgram.Create(waterVertexShader, waterFragmentShader);
}

void GenerationApp::GenerateWaterTexture()
{
	int imageWidth = 0, imageHeight = 0, imageFormat = 0;

	unsigned char* data = stbi_load("..\\data\\textures\\water_d.jpg", &imageWidth, &imageHeight, &imageFormat, STBI_default);
	// this is for texturing over the perlin noise [water texture]
	glActiveTexture(GL_TEXTURE2);
	glGenTextures(1, &m_water_texture);
	glBindTexture(GL_TEXTURE_2D, m_water_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	stbi_image_free(data);
}

void GenerationApp::BindTexturesAndUniforms(GLuint programID, glm::vec3 flycamPos)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_perlin_texture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_ground_texture);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_snow_texture);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_stone_texture);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, m_sand_texture);

	//Setting the sampler lookup slot
	int iLocation = glGetUniformLocation(programID, "perlin_texture");
	assert(iLocation != -1 && "If -1, we cannot find the Perlin Texture.");
	glUniform1i(iLocation, 0);

#pragma region Texture Bindings
	//Setting up the ground texture sampler
	iLocation = glGetUniformLocation(programID, "textureGround");
	glUniform1i(iLocation, 1);
		
	//setting up the snow texture sampler
	glBindTexture(GL_TEXTURE_2D, m_snow_texture);
	iLocation = glGetUniformLocation(programID, "textureSnow");
	assert(iLocation != -1 && "If -1, we cannot find the Snow texture.");
	glUniform1i(iLocation, 3);

	//setting up the stone texture sampler
	glBindTexture(GL_TEXTURE_2D, m_stone_texture);
	iLocation = glGetUniformLocation(programID, "textureStone");
	assert(iLocation != -1 && "If -1, we cannot find the Stone texture.");
	glUniform1i(iLocation, 4);

	//setting up the sand texture sampler
	glBindTexture(GL_TEXTURE_2D, m_sand_texture);
	iLocation = glGetUniformLocation(programID, "textureSand");
	assert(iLocation != -1 && "If -1, we cannot find the Stone texture.");
	glUniform1i(iLocation, 5);

#pragma endregion

#pragma region Lighting Tools
	//setting up the perlin noise ambient lighting for the GUI
	iLocation = glGetUniformLocation(programID, "kA");
	assert(iLocation != -1 && "If -1, we cannot find the kA Uniform.");
	glUniform3fv(iLocation, 1, glm::value_ptr(BaseApplication::gui_GenAmbientColour));

	//setting up the perlin noise diffuse lighting for the GUI
	iLocation = glGetUniformLocation(programID, "kD");
	assert(iLocation != -1 && "If -1, we cannot find the kD Uniform.");
	glUniform3fv(iLocation, 1, glm::value_ptr(BaseApplication::gui_GenDiffuseColour));

	//setting up the perlin noise lighting direction for the GUI
	iLocation = glGetUniformLocation(programID, "LightDirection");
	assert(iLocation != -1 && "If -1, we cannot find the Light Direction uniform.");
	glUniform3fv(iLocation, 1, glm::value_ptr(BaseApplication::gui_GenLightDirection));
#pragma endregion
}

void GenerationApp::BindWaterTexturesAndUniforms(GLuint programID)
{
	/* THIS IS FOR THE WATER WHICH NEEDS TO GO AFTER THE PERLIN NOISE GENERATION */
	
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_water_texture);

	// Checking the uniform to find the water texture
	int waterILocation = glGetUniformLocation(programID, "textureWater");
	assert(waterILocation != -1 && "If -1, we cannot find the Water Texture.");
	glUniform1i(waterILocation, 2);

	// Checking the uniform to find the water height variable
	waterILocation = glGetUniformLocation(programID, "waterHeight");
	assert(waterILocation != -1 && "If -1, we cannot find the waterHeight uniform.");
	glUniform1f(waterILocation, BaseApplication::gui_WaterHeight);

	// Checking the uniform to find the wave height scale variable
	waterILocation = glGetUniformLocation(programID, "heightScale");
	assert(waterILocation != -1 && "If -1, we cannot find the heightScale uniform.");
	glUniform1f(waterILocation, BaseApplication::gui_WaterHeightScale);

	// Checking the uniform to find the time variable
	waterCounter += DT;
	waterILocation = glGetUniformLocation(programID, "time");
	assert(waterILocation != -1 && "If -1, we cannot find the Time uniform.");
	glUniform1f(waterILocation, waterCounter);

	// Checking the uniform to find the opacity variable
	waterILocation = glGetUniformLocation(programID, "opaqueLevel");
	assert(waterILocation != -1 && "If -1, we cannot find the opaqueLevel uniform.");
	glUniform1f(waterILocation, BaseApplication::gui_WaterOpacity);	
}