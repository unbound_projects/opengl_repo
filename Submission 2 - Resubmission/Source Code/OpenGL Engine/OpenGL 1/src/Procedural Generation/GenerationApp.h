#pragma once
#include "../GameObject Class/BaseObject.h"
#include "../Program Helper Class/ProgramHelper.h"
#include "gl_core_4_4.h"
#include <glm\vec2.hpp>
#include <glm\vec3.hpp>

class GenerationApp : public BaseObject
{
public:
	struct Vertex
	{
		glm::vec4 position;
		glm::vec4 colour;
		glm::vec2 texCoord;
		glm::vec2 texCoord2;
	};

	// BaseObject pure virtual function
	bool Initialise();
	void DeInitialise();
	bool Update(float deltaTime);
	void Draw(glm::mat4x4 projectionView);

	// Function used to generate a height map
	void GenerateHeightMap(GLuint rows, GLuint columns);

	// Function used to generate perlin noise
	void GeneratePerlinNoise();

	// Function used to RE-generate the perlin noise (for GUI)
	void RegenerateNoise();

	// Function used to generate textures for the perlin noise
	void GenerateTextures();

	// Function used to generate shaders for the perlin noise
	void GenerateShaders();

	// Function used to generate shaders for the water grid
	void GenerateWaterShader();

	// Function used to generate the texture for the water grid
	void GenerateWaterTexture();

	// Function used to bind the textures and uniforms for the perlin noise
	void BindTexturesAndUniforms(GLuint programID, glm::vec3 flycamPos);
	
	// Function used to bind the textures and uniforms for the water grid
	void BindWaterTexturesAndUniforms(GLuint programID);

private:
	ProgramHelper m_program;
	ProgramHelper m_waterProgram;
	GLuint m_perlin_texture;

	//TEXTURE VARIABLES
	GLuint m_ground_texture;
	GLuint m_water_texture;
	GLuint m_snow_texture;
	GLuint m_stone_texture;
	GLuint m_sand_texture;

	float m_seed;

	double DT = 0;
	double pastFrame;
	float heightScale = 0;
	float waterCounter = 0;

	float* perlin_data;

	// Buffers
	GLuint m_ProceduralVAO;
	GLuint m_ProceduralVBO;
	GLuint m_ProceduralIBO;
	GLuint m_WaterVAO;
	GLuint m_WaterVBO;
	GLuint m_WaterIBO;

	unsigned int m_uiGridIndexCount;
};